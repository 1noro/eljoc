# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 0.0.6 - 2020-03-31
### Added
- Agregada la función showExchangeOptionsWithBank al PopUpPannel
### Changed
- Modificado el estilo de showExchangeOptionsWithBank
- Ahora conservamos el click en el popup showExchangeOptionsWithBank
- Reestructurado el sideLeft del showExchangeOptionsWithBank
- Remodelada la parte gráfica del showExchangeOptionsWithBank
- Separada la función showExchangeOptionsWithBank de PopUpPannel y movida a su propia clase BankExchangeMenu

## 0.0.5 - 2020-03-30
### Added
- Agregada la función showWonResourceCards al PopUpPannel
- Agregada la función showWonDevelopmentCard al PopUpPannel
- Agregada la función de close y simpleClose a PopUpPannel
- Agregada la función que te devuelve las descripcciones de las cartas de desarrollo
- Agregada la función showOpponentsToSteal al PopUpPannel
### Changed
- Cambiados los nombres de las cartas de desarrollo

## 0.0.4 - 2020-03-29
### Added
- Implementada la clase DevelopmentCardItem en el LeftPanel de la misma forma que ResourceCardItem
- Agregada una funcionabilidad de switch al LeftPanel para intercambiar la visibilidad entre el panel de recursos y desarrollo
- Aplicado GUI como interfaz para agregar o quitar cartas del LeftPanel
- Agregada una pantalla de carga al principio de todo que se oculta automáticamente cuando se carga el logo del formulario de inicio
- Agregada la funcionabilida básica del popup
### Changed
- Ahora TopPannel genera sus propios HTML objects
- Ahora MiddlePannel genera y controla sus propios HTML objects correctamente
- Ahora RightPannel genera y controla sus propios HTML objects correctamente
- El LeftPannel se muestra cuando corresponde

## 0.0.3 - 2020-03-28
### Added
- Agregadas un montón de link y meta tags al html para que la pagina responda a aplicaciones de terceros de manera correcta
- Agregada la clase CLI por si me hace falta un interfaz de comunicación con el usuario a través de la terminal del navegador (previsión de futuro)
- Agregada una función a la clase CLI que permite el borrado total de la base de datos forzando así que todas las paginas (con partidas iniciadas) se reinicien
- Agregada variable de testing que permite realizar accines en función de ella, como por ejemplo, predefinir valores al formulario de logi o mostrar la información de debugeo de la aplicación en el panel derecho
- Agregada la funcionabilidad de agregar y sustraer cartas de recursos del LeftPanel de forma dinámica y actualizando el número total de cartas en mano
- Establecido un código para cada una de las cartas de desarrollo
### Changed
- Separado el loginForm y el waitingGif del board_canvas_cont y puestos en el middle_pannel
- Ahora Board controla boardCanvasContDiv
- Ahora en la clase Game ya no se utiliza la clase GUI para obtener los paneles sino que se utiliza como interfaz hacia sus clases hijas (los paneles)
- Ahora en LoginForm se utiliza GUI como interfaz para los elementos gráficos
- Nueva organización de la clase ClickBehavior gracias a usar GUI como interfaz, pero hace falta una limpieza mas profunda
- Ahora LoginForm crea su propia estructura de divs y se controla a si mismo
- Ahora el panel izquierdo está generado dinámicamente por las clases correspondientes

## 0.0.2 - 2020-03-27
### Added
- Creada la carpeta GUI y su clase asociada (por ahora vacía)
- Definida la estructura de la clase GUI y creada una clase para cada panel
- Creadas las clases de todos los nuevos agregados al principio de esta redistribución
- Definida y funcionando la clase TopPannel
### Changed
- mv public_html/js/board/ public_html/js/GUI/board/
- mv public_html/js/pannel/LoginForm.js public_html/js/GUI/middle-pannel/LoginForm.js
- mv public_html/js/pannel/BottomPannel.js public_html/js/GUI/BottomPannel.js
- mv public_html/js/pannel/LeftPannel.js public_html/js/GUI/LeftPannel.js
- mv public_html/js/pannel/RightPannel.js public_html/js/GUI/RightPannel.js
- mv public_html/js/GUI/board/Board.js public_html/js/GUI/Board.js
- mv public_html/js/GUI/GUI.js public_html/js/GUI.js
- Movida la función getCursolRelAndDoSomething de cursor-actions.js a cursor-utils.js
- mv public_html/js/game/Game.js public_html/js/Game.js
- window.joc.game > window.g
- Reorganizada la primera parte del main
- Reestructurada la clase LoginForm
- Reorganizado el constructor de la clase Game
- Implementada completamente la clase ClickBehavior dentro de Game
- Implementada la clase LoginForm en MiddlePannel y esta en GUI para que no se tengan que guardar tantas variables publicas
- Implementada la clase BottomPannel en GUI para que no se tengan que guardar tantas variables publicas
- La clase Opponent ha sido reimplementada en la clase OpponentItem y su funcionabilidad ha sido transferida RightPannel
- Ahora mostrar gameInfo en gameInfoContDiv está gestionado ppor la clase RightPannel
- Ahora el permiso de clickar los dados es gestionado por clickBehavior
- Ahora Board se genera en GUI y no en Game
- Ahora Board depende exclusivamente de la clase GUI
- Todos los elementos (arrays de elementos) del board se controlan desde la propia clase Board
### Removed
- Eliminadas funciones innecesarias de la clase Game y movidas al apartado de utils (isDefinedNumber, findGetParameter, checkAndGetParamGET)
- rm -r public_html/js/actions/cursor-actions.js

## 0.0.1 - 2020-03-26
### Added
- Funciona todo hasta el final de la fase 4
- Ahora los pueblos saben que recursos dan cada uno
- Guardamos el último pueblo de cada jugador en la fase 4 para saber cuales son sus recursos iniciales
- Ahora solo se añade un color a allowedColors si no estaba previamente
- Agregada la función getDataForTownsSQLArr a las ciudades, con el fin de simplificar la actualización del array "towns" que se le manda al SQL puede que sea útil para las carreteras (d/v)
### Changed
- Ahora los pueblos guardan en la base de datos la lista de allowedColors pero ignoran esa lista siempre que estén en la fase 3 o 4
- Cambio de localización de la clase LoginForm
- Movidos los tests de php a la carpeta genérica tests
### Fixed
- Las carreteras solo se pueden poner al lado de uno de los pueblos de tu mismo color
- Las carreteras que se ponen a 2 de distancia de un pueblo habilitan que pongas un pueblo en la posición a la que llegan

[Unreleased]: https://gitlab.com/1nor0/eljoc
