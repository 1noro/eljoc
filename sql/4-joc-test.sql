
USE JOC;

/*
 * PARA VER LOS EVENTOS ACTIVOS
 *
 * SHOW PROCESSLIST;
 *
 */
/*
CALL insert_player;
CALL insert_player;
CALL insert_player;

CALL ping_from_user(2);
CALL ping_from_user(2);

SELECT * FROM player;
*/

-- CALL hello;

-- ESCENARIO 1
CALL insert_defined_player(1);
CALL insert_defined_player(2);
CALL insert_defined_player(3);
CALL insert_defined_player(4);

CALL insert_defined_player(11);
CALL insert_defined_player(12);
CALL insert_defined_player(13);
CALL insert_defined_player(14);

CALL insert_defined_player(21);
CALL insert_defined_player(22);
CALL insert_defined_player(23);
CALL insert_defined_player(24);

CALL insert_defined_game(1);
CALL insert_defined_game(2);
CALL insert_defined_game(3);

-- PARTIDA 1
CALL join_game(1, 1, @ok);
CALL join_game(2, 1, @ok);
CALL join_game(3, 1, @ok);
CALL join_game(4, 1, @ok);

CALL set_ready(1, 1);
CALL set_ready(2, 1);
CALL set_ready(3, 1);
CALL set_ready(4, 1);

-- PARTIDA 2
CALL join_game(11, 2, @ok);
CALL join_game(12, 2, @ok);
CALL join_game(13, 2, @ok);
-- CALL join_game(14, 2, @ok);

CALL set_ready(11, 2);
CALL set_ready(12, 2);
CALL set_ready(13, 2);
-- CALL set_ready(14, 2);

-- PARTIDA 3
CALL join_game(21, 3, @ok);
CALL join_game(22, 3, @ok);
-- CALL join_game(23, 3, @ok);
-- CALL join_game(24, 3, @ok);

CALL set_ready(21, 3);
CALL set_ready(22, 3);
-- CALL set_ready(23, 3);
-- CALL set_ready(24, 3);

SELECT DISTINCT pg.id_game AS id_game
    FROM player_game AS pg, game AS g
    WHERE g.id = pg.id_game AND g.waiting = 1 AND
        (SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 
            (SELECT COUNT(id_player) FROM player_game WHERE pg.id_game = id_game GROUP BY id_game) AND 
        ((SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 3 OR 
            (SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 4);
        
SELECT COUNT(DISTINCT pg.id_game)
    FROM player_game AS pg, game AS g
    WHERE g.id = pg.id_game AND g.waiting = 1 AND
        (SELECT COUNT(id_player)
            FROM player_game
            WHERE ready = 1 AND pg.id_game = id_game
            GROUP BY id_game) >= 3;

DELIMITER $$
    DROP PROCEDURE IF EXISTS start_ready_games;$$
    CREATE PROCEDURE start_ready_games() BEGIN
        DECLARE games_to_start INT;
        DECLARE done INT DEFAULT FALSE;
        DECLARE my_id_game INT;
    
        DECLARE c1 CURSOR FOR
            SELECT DISTINCT pg.id_game AS id_game
            FROM player_game AS pg, game AS g
            WHERE g.id = pg.id_game AND g.waiting = 1 AND
                (SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 
                    (SELECT COUNT(id_player) FROM player_game WHERE pg.id_game = id_game GROUP BY id_game) AND 
                ((SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 3 OR 
                    (SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 4);
                    
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                    
        SET games_to_start = 0;
        SELECT COUNT(DISTINCT pg.id_game) INTO games_to_start
            FROM player_game AS pg, game AS g
            WHERE g.id = pg.id_game AND g.waiting = 1 AND
                (SELECT COUNT(id_player)
                    FROM player_game
                    WHERE ready = 1 AND pg.id_game = id_game
                    GROUP BY id_game) >= 3;
        
        IF (games_to_start != 0) THEN
            
            OPEN c1;
            myloop: LOOP
                FETCH c1 INTO my_id_game;
                IF done THEN
                    LEAVE myloop;
                END IF;
    
                UPDATE game
                    SET waiting = 0, start_date = NOW()
                    WHERE id = my_id_game;
    
            END LOOP;
            CLOSE c1;
        END IF;
    END;$$
DELIMITER ;

-- CALL start_ready_games; SELECT * FROM game;

SELECT * FROM player_game;
SELECT * FROM game;
