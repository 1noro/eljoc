
USE JOC;

-- TABLAS ----------------------------------------------------------------
DROP TABLE IF EXISTS tmp_data;
DROP TABLE IF EXISTS player_game;
DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS player;

CREATE TABLE player (
    id integer not null PRIMARY KEY,
    creation_date datetime not null DEFAULT NOW(),
    online bit not null DEFAULT 0, -- cambiar al final para 1 (o no)
    last_ping datetime not null DEFAULT NOW()
);

CREATE TABLE game (
    id integer not null PRIMARY KEY,
    spaces integer not null DEFAULT 4,
    waiting bit not null DEFAULT 1,
    finished bit not null DEFAULT 0,
    phase integer not null DEFAULT 0,
    game_round integer not null DEFAULT 0,
    game_order varchar(50) DEFAULT null,
    towns varchar(1600) DEFAULT '[[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]],[[0,0,0,[]],[0,0,0,[]],[0,0,0,[]]]]',
    droads varchar(700) DEFAULT '[[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]]]',
    vroads varchar(350) DEFAULT '[[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]],[0,[]]]',
    creation_date datetime not null DEFAULT NOW(),
    start_date datetime DEFAULT null,
    expiration_date datetime not null DEFAULT DATE_ADD(NOW(), INTERVAL 1 DAY) -- valor provisional, luego incrementar
);

CREATE TABLE player_game (
    id integer not null PRIMARY KEY,
    id_player integer not null,
    id_game integer not null,
    color integer not null,
    ready bit not null DEFAULT 0,
    turn bit not null DEFAULT 0,
    FOREIGN KEY (id_player) REFERENCES player(id),
    FOREIGN KEY (id_game) REFERENCES game(id)
);

CREATE TABLE tmp_data (
    id_game integer not null,
    id_player integer not null,
    data_tag varchar(20) not null,
    tmp_data text,
    FOREIGN KEY (id_game) REFERENCES game(id),
    FOREIGN KEY (id_player) REFERENCES player(id),
    PRIMARY KEY (id_game, id_player, data_tag)
);

-- PROCEDURES ----------------------------------------------------------------

DELIMITER $$

    DROP PROCEDURE IF EXISTS insert_player;$$
    CREATE PROCEDURE insert_player() BEGIN
        SET @last_id = 0;
        SELECT id INTO @last_id FROM player ORDER BY id DESC LIMIT 1;
        IF (@last_id IS NULL) THEN
            SET @last_id = 0;
        END IF;
        INSERT INTO player (id) VALUES (@last_id + 1);
    END;$$
    
    DROP PROCEDURE IF EXISTS insert_defined_player;$$
    CREATE PROCEDURE insert_defined_player(IN _player_id integer) BEGIN
        INSERT INTO player (id) VALUES (_player_id);
    END;$$

    DROP PROCEDURE IF EXISTS insert_game;$$
    CREATE PROCEDURE insert_game() BEGIN
        SET @last_id = 0;
        SELECT id INTO @last_id FROM game ORDER BY id DESC LIMIT 1;
        IF (@last_id IS NULL) THEN
            SET @last_id = 0;
        END IF;
        INSERT INTO game (id) VALUES (@last_id + 1);
    END;$$
    
    DROP PROCEDURE IF EXISTS insert_defined_game;$$
    CREATE PROCEDURE insert_defined_game(IN _game_id integer) BEGIN
        INSERT INTO game (id) VALUES (_game_id);
    END;$$
    
    DROP PROCEDURE IF EXISTS get_game_status;$$
    CREATE PROCEDURE get_game_status(IN _game_id integer) BEGIN
        SELECT pg.id_player AS id_player, pg.color AS color, p.online AS online, pg.ready AS ready, pg.turn AS turn
            FROM player_game AS pg, player AS p 
            WHERE p.id = pg.id_player AND pg.id_game = _game_id;
    END;$$

    DROP PROCEDURE IF EXISTS ping_from_player;$$
    CREATE PROCEDURE ping_from_player(IN _player_id integer) BEGIN
        UPDATE player SET online = 1, last_ping = NOW() WHERE id = _player_id;
    END;$$

    DROP PROCEDURE IF EXISTS check_online;$$
    CREATE PROCEDURE check_online(IN _player_id integer, OUT _is_online bit) BEGIN
        SELECT online INTO _is_online FROM player WHERE id = _player_id LIMIT 1;
    END;$$

    DROP PROCEDURE IF EXISTS set_offline;$$
    CREATE PROCEDURE set_offline(IN _player_id integer) BEGIN
        UPDATE JOC.player SET online = 0 WHERE id = _player_id;
    END;$$

    DROP PROCEDURE IF EXISTS set_online;$$
    CREATE PROCEDURE set_online(IN _player_id integer) BEGIN
        UPDATE JOC.player SET online = 1 WHERE id = _player_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS set_ready;$$
    CREATE PROCEDURE set_ready(IN _player_id integer, IN _game_id integer) BEGIN
        UPDATE player_game SET ready = 1 WHERE id_player = _player_id AND id_game = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS unset_ready;$$
    CREATE PROCEDURE unset_ready(IN _player_id integer, IN _game_id integer) BEGIN
        UPDATE player_game SET ready = 0 WHERE id_player = _player_id AND id_game = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS clean_game_players;$$
    CREATE PROCEDURE clean_game_players(IN _game_id integer) BEGIN
        DELETE FROM player_game WHERE id_game = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS request_turn;$$
    CREATE PROCEDURE request_turn(IN _player_id integer, IN _game_id integer) BEGIN
        UPDATE player_game SET turn = 0 WHERE id_game = _game_id;
        UPDATE player_game SET turn = 1 WHERE id_player = _player_id AND id_game = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS next_phase;$$
    CREATE PROCEDURE next_phase(IN _game_id integer) BEGIN
        UPDATE game SET phase = phase + 1, game_round = 0 WHERE id = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS next_game_round;$$
    CREATE PROCEDURE next_game_round(IN _game_id integer) BEGIN
        UPDATE game SET game_round = game_round + 1 WHERE id = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS set_order;$$
    CREATE PROCEDURE set_order(IN _game_id integer, IN _game_order varchar(50)) BEGIN
        UPDATE game SET game_order = _game_order WHERE id = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS set_towns;$$
    CREATE PROCEDURE set_towns(IN _game_id integer, IN _towns_array varchar(800)) BEGIN
        UPDATE game SET towns = _towns_array WHERE id = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS set_droads;$$
    CREATE PROCEDURE set_droads(IN _game_id integer, IN _droads_array varchar(700)) BEGIN
        UPDATE game SET droads = _droads_array WHERE id = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS set_vroads;$$
    CREATE PROCEDURE set_vroads(IN _game_id integer, IN _vroads_array varchar(350)) BEGIN
        UPDATE game SET vroads = _vroads_array WHERE id = _game_id;
    END;$$
    
    DROP PROCEDURE IF EXISTS start_game;$$
    CREATE PROCEDURE start_game(IN _player_id integer, IN _game_id integer, IN _game_order varchar(50)) BEGIN
        START TRANSACTION;
            CALL set_order(_game_id, _game_order);
            CALL request_turn(_player_id, _game_id);
            CALL next_phase(_game_id);
        COMMIT;
    END;$$
    
    DROP PROCEDURE IF EXISTS register_tmp_data;$$
    CREATE PROCEDURE register_tmp_data(IN _game_id integer, IN _player_id integer, IN _data_tag varchar(20), IN _tmp_data text) BEGIN
        DECLARE _num_rows INT DEFAULT 0;
        SET _num_rows = 0;
        SELECT COUNT(data_tag) INTO _num_rows FROM tmp_data WHERE id_game = _game_id AND  id_player = _player_id AND data_tag = _data_tag;
        IF _num_rows = 0 THEN
            INSERT INTO tmp_data (id_game, id_player, data_tag, tmp_data) VALUES (_game_id, _player_id, _data_tag, _tmp_data);
        ELSE
            UPDATE tmp_data SET tmp_data = _tmp_data WHERE id_game = _game_id AND  id_player = _player_id AND data_tag = _data_tag;
        END IF;
    END;$$
    
    DROP PROCEDURE IF EXISTS get_turn_dices;$$
    CREATE PROCEDURE get_turn_dices(IN _game_id integer) BEGIN
        SELECT id_player, tmp_data FROM tmp_data WHERE id_game = _game_id AND data_tag = 'turn_dice_value';
    END;$$
    
    DROP PROCEDURE IF EXISTS end_phase2;$$
    CREATE PROCEDURE end_phase2(
        IN _game_id integer,
        IN _next_player_id integer, 
        IN _new_order varchar(50)
    ) BEGIN
        START TRANSACTION;
            CALL set_order(_game_id, _new_order);
            CALL next_phase(_game_id);
            CALL request_turn(_next_player_id, _game_id); 
        COMMIT;
    END;$$
    
    DROP PROCEDURE IF EXISTS delete_all_data;$$
    CREATE PROCEDURE delete_all_data() BEGIN
        START TRANSACTION;
            DELETE FROM tmp_data;
            DELETE FROM player_game;
            DELETE FROM game;
            DELETE FROM player;
        COMMIT;
    END;$$
    
    -- join_game ---------------------------------------------------------------------------

    DROP PROCEDURE IF EXISTS join_game;$$
    CREATE PROCEDURE join_game(IN _player_id integer, IN _game_id integer, OUT _allowed bit) BEGIN
        SET _allowed = 0;
        SET @already_joined = 0;
        SET @occupied_spaces = 0;
        SET @available_spaces = 0;
        SET @game_waiting = 0;
        SET @game_finished = 0;
    
        -- SI LA PARTIDA O EL USUARIO NO ESTAN CREADOS, LOS CREAMOS
        IF ((SELECT COUNT(id) FROM player WHERE id = _player_id) = 0) THEN CALL insert_defined_player(_player_id); END IF;
        IF ((SELECT COUNT(id) FROM game WHERE id = _game_id) = 0) THEN CALL insert_defined_game(_game_id); END IF;
    
        -- AHORA HACEMOS LAS COMPROBACIONES
        SELECT COUNT(id_player) INTO @already_joined
            FROM player_game WHERE id_game = _game_id AND id_player = _player_id;
    
        IF (@already_joined = 0) THEN
        
            SELECT COUNT(id_player) INTO @occupied_spaces
                FROM player_game
                WHERE id_game = _game_id;
            
            SELECT spaces, waiting, finished INTO @available_spaces, @game_waiting, @game_finished FROM game WHERE id = _game_id;
        
            IF (@occupied_spaces < @available_spaces AND @game_waiting = 1 AND @game_finished = 0) THEN
                SET @last_id = 0;
                SELECT id INTO @last_id FROM player_game ORDER BY id DESC LIMIT 1;
                IF (@last_id IS NULL) THEN
                    SET @last_id = 0;
                END IF;
                INSERT INTO player_game (id, id_player, id_game, color) VALUES (@last_id + 1, _player_id, _game_id, @occupied_spaces + 1);
                SET _allowed = 1;
            END IF;
        ELSE
            SET _allowed = 1;
        END IF;
    
    END;$$
    
    -- start_ready_games ---------------------------------------------------------------------------
    
        DROP PROCEDURE IF EXISTS start_ready_games;$$
    CREATE PROCEDURE start_ready_games() BEGIN
        DECLARE games_to_start INT;
        DECLARE done INT DEFAULT FALSE;
        DECLARE my_id_game INT;
    
        DECLARE c1 CURSOR FOR
            SELECT DISTINCT pg.id_game AS id_game
            FROM player_game AS pg, game AS g
            WHERE g.id = pg.id_game AND g.waiting = 1 AND
                (SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 
                    (SELECT COUNT(id_player) FROM player_game WHERE pg.id_game = id_game GROUP BY id_game) AND 
                ((SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 3 OR 
                    (SELECT COUNT(id_player) FROM player_game WHERE ready = 1 AND pg.id_game = id_game GROUP BY id_game) = 4);
                    
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
                    
        SET games_to_start = 0;
        SELECT COUNT(DISTINCT pg.id_game) INTO games_to_start
            FROM player_game AS pg, game AS g
            WHERE g.id = pg.id_game AND g.waiting = 1 AND
                (SELECT COUNT(id_player)
                    FROM player_game
                    WHERE ready = 1 AND pg.id_game = id_game
                    GROUP BY id_game) >= 3;
        
        IF (games_to_start != 0) THEN
            
            OPEN c1;
            myloop: LOOP
                FETCH c1 INTO my_id_game;
                IF done THEN
                    LEAVE myloop;
                END IF;
    
                UPDATE game
                    SET waiting = 0, start_date = NOW()
                    WHERE id = my_id_game;
                
                CALL next_phase(my_id_game); 
    
            END LOOP;
            CLOSE c1;
        END IF;
    END;$$

DELIMITER ;

-- EVENTS ----------------------------------------------------------------

DELIMITER $$

    DROP EVENT IF EXISTS check_online_event;$$
    CREATE EVENT check_online_event
        ON SCHEDULE EVERY 10 SECOND -- 10 SECOND
        DO BEGIN
            SIGNAL SQLSTATE '01000' SET MESSAGE_TEXT = 'check_online started';
        
            -- PONEMOS ONLINE A LOS QUE HAN HECHO PING
            UPDATE JOC.player
                SET online = 0
                WHERE DATE_ADD(last_ping, INTERVAL 60 SECOND) < NOW(); -- 60 SECOND
            
            -- INICIAMOS LAS PARTIDAS QUE ESTÉN LISTAS
            CALL start_ready_games;
        
            SIGNAL SQLSTATE '01000' SET MESSAGE_TEXT = 'check_online finished';
        END;$$

DELIMITER ;

-- TESTS ----------------------------------------------------------------

/*CALL insert_player;
CALL insert_player;
CALL insert_player;
CALL insert_player;
CALL insert_player;

CALL insert_game;
CALL insert_game;*/

/*

CALL join_game(1, 1, @ok);
-- SELECT @ok;
CALL join_game(2, 1, @ok);
-- SELECT @ok;
CALL join_game(3, 1, @ok);
-- SELECT @ok;
CALL join_game(4, 1, @ok);
-- SELECT @ok;
CALL join_game(5, 1, @ok);
-- SELECT @ok;
CALL join_game(5, 2, @ok);
-- SELECT @ok;

CALL get_game_status(10);

SELECT * FROM player;
SELECT * FROM game;
SELECT * FROM player_game;
SELECT * FROM tmp_data;

CALL set_ready(55, 10);
CALL unset_ready(11, 10);

CALL set_offline(1);

CALL get_turn_dices(10);

CALL delete_all_data;

SELECT ~0; -- max int

CREATE VIEW a
as
    select id from player
GO

select * from a

*/

