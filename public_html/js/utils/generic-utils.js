
function isDefinedNumber(_in) {
    var out = false;
    if (_in && Number.isInteger(parseInt(_in))) out = true;
    return out;
}

function generateDiv(id, className) {
    var div = document.createElement("div");
    div.id = id;
    div.className = className;
    return div;
}

function generateImg(id, className, src) {
    var img = document.createElement("img");
    img.id = id;
    img.className = className;
    img.src = src;
    return img;
}

function generateInlineArrow(id, left) {
    var arrowSpan = document.createElement("span");
    arrowSpan.id = id;
    arrowSpan.className = "inline_arrow " + (left ? "left" : "right");
    return arrowSpan;
}

function generateInlineSpaceSeparator() {
    var arrowSpan = document.createElement("span");
    arrowSpan.className = "inline_separator";
    arrowSpan.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;";
    return arrowSpan;
}

function generateItem(id) {
    return generateDiv(id, "item");
}
