
function readFromPHP(execFunction, dataJSON) {
    window.joc.waiting_for_http = true;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            try {
                var json = JSON.parse(atob(this.responseText));

                if (json["error"])
                    for (var i = 0; i < json["error"].length; i++)
                        console.log("[FAIL] " + json["error"][i]);

                if (json["info"])
                    for (var i = 0; i < json["info"].length; i++)
                        console.log("[INFO] " + json["info"][i]);

                window.g.analyzeData(json["exec"], json["data"]);
            } catch (error) {
                console.error("[FAIL] El texto recibido desde el PHP no está correctamente encriptado en base64");
                console.error(error);
                console.error("" + this.responseText);
            }

            window.joc.waiting_for_http = false;

        }
    };
    xhttp.open("POST", "php/main.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("exec=" + execFunction + "&data=" + btoa(dataJSON));
}
