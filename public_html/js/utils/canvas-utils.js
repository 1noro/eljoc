
function resetCanvasWithBgColor(ctx, rbw, rbh, bgColor) {
    ctx.fillStyle = bgColor;
    ctx.fillRect(0, 0, rbw, rbh);
}
