
// traducción de un array lineal al array bidimensional
function num2posTown(num) {
    var f = 0, c = 0;

    if (num - 3 < 0) {f = 0; c = num;}
    else if (num - 7 < 0) {f = 1; c = num - 3;}
    else if (num - 11 < 0) {f = 2; c = num - 7;}
    else if (num - 16 < 0) {f = 3; c = num - 11;}
    else if (num - 21 < 0) {f = 4; c = num - 16;}
    else if (num - 27 < 0) {f = 5; c = num - 21;}
    else if (num - 33 < 0) {f = 6; c = num - 27;}
    else if (num - 38 < 0) {f = 7; c = num - 33;}
    else if (num - 43 < 0) {f = 8; c = num - 38;}
    else if (num - 47 < 0) {f = 9; c = num - 43;}
    else if (num - 51 < 0) {f = 10; c = num - 47;}
    else if (num - 54 < 0) {f = 11; c = num - 51;};

    return {"f" : f, "c" : c};
}

function posTown2num(f, c) {
    var num = 0;

    if (f == 0) num = c;
    else if (f == 1) num = c + 3;
    else if (f == 2) num = c + 7;
    else if (f == 3) num = c + 11;
    else if (f == 4) num = c + 16;
    else if (f == 5) num = c + 21;
    else if (f == 6) num = c + 27;
    else if (f == 7) num = c + 33;
    else if (f == 8) num = c + 38;
    else if (f == 9) num = c + 43;
    else if (f == 10) num = c + 47;
    else if (f == 11) num = c + 51;

    return num;
}

function getBlockedTowns(f0, c0) {
    var out = new Array();
    var f1 = 0, c1 = 0;
    var f2 = 0, c2 = 0;
    var f3 = 0, c3 = 0;

    var v = [
        [8, 8, 8],
        [8, 8, 8, 8],
        [8, 8, 8, 8],
        [8, 8, 8, 8, 8],
        [8, 8, 8, 8, 8],
        [8, 8, 8, 8, 8, 8],
        [8, 8, 8, 8, 8, 8],
        [8, 8, 8, 8, 8],
        [8, 8, 8, 8, 8],
        [8, 8, 8, 8],
        [8, 8, 8, 8],
        [8, 8, 8]
    ];

    if (f0 % 2 != 0) {
        // hay que ir hacia arriba con 2 y hacia abajo con 1
        f1 = f0 - 1;
        c1 = (f0 <= 5) ? c0 - 1 : c0 + 1;

        f2 = f0 - 1;
        c2 = c0;

        f3 = f0 + 1;
        c3 = c0;
    } else {
        // hay que ir hacia arriba con 1 y hacia abajo con 2
        f1 = f0 - 1;
        c1 = c0;

        f2 = f0 + 1;
        c2 = c0;

        f3 = f0 + 1;
        c3 = (f0 <= 5) ? c0 + 1 : c0 - 1;
    }

    // asignamos 1 a el pueblo y 2 a los bloqueados
    if (v[f1]) if (v[f1][c1]) out.push({"f" : f1, "c" : c1});
    if (v[f2]) if (v[f2][c2]) out.push({"f" : f2, "c" : c2});
    if (v[f3]) if (v[f3][c3]) out.push({"f" : f3, "c" : c3});

    return out;
}

// -----------------------------------------------------------------------------
function posTown2hexNumList(f, c) {
    var out = new Array();
    for (var i = 0; i < window.joc.hexTown.length; i++) {
        for (var e = 0; e < window.joc.hexTown[i].length; e++) {
            var _f = window.joc.hexTown[i][e][0];
            var _c = window.joc.hexTown[i][e][1];
            if (f == _f && c == _c) {
                out.push(i);
                break;
            }
        }
    }
    return out;
}

// -----------------------------------------------------------------------------

function nRoad2Towns(arr, nVRoad) {
    return arr[nVRoad];
}

function townFC2nDRoad(f, c) {
    var out = new Array();
    for (var i = 0; i < window.joc.dRoadTown.length; i++) {
        for (var e = 0; e < window.joc.dRoadTown[i].length; e++) {
            var myF = window.joc.dRoadTown[i][e][0];
            var myC = window.joc.dRoadTown[i][e][1];
            if (f == myF && c == myC) {
                out.push(i);
                break;
            }
        }
    }
    return out;
}

function townFC2nVRoad(f, c) {
    var out = 0;
    for (var i = 0; i < window.joc.vRoadTown.length; i++) {
        for (var e = 0; e < window.joc.vRoadTown[i].length; e++) {
            var myF = window.joc.vRoadTown[i][e][0];
            var myC = window.joc.vRoadTown[i][e][1];
            if (f == myF && c == myC) {
                out = i;
                break;
            }
        }
    }
    return out;
}

// -----------------------------------------------------------------------------

function getResourceStr(_num) {
    var out = "";
    switch (parseInt(_num)) {
        case 0:
            out = "Desierto";
            break;
        case 1:
            out = "Trigo";
            break;
        case 2:
            out = "Piedra";
            break;
        case 3:
            out = "Oveja";
            break;
        case 4:
            out = "Arcilla";
            break;
        case 5:
            out = "Madera";
            break;
        default:
            out = "Desconocido";
            break;
    }
    return out;
}

function getDevelopmentStr(_num) {
    var out = "";
    switch (parseInt(_num)) {
        case 0:
            out = "Cabllero";
            break;
        case 1:
            out = "Carreteras";
            break;
        case 2:
            out = "Invento";
            break;
        case 3:
            out = "Punto de victoria";
            break;
        case 4:
            out = "Monopolio";
            break;
        default:
            out = "Desconocido";
            break;
    }
    return out;
}

function getDevelopmentDesc(_num) {
    var out = "";
    switch (parseInt(_num)) {
        case 0:
            out = "Desplaza el ladrón y roba una carta a uno de los jugadores afectados.";
            break;
        case 1:
            out = "Construye dos carreteras sin coste alguno.";
            break;
        case 2:
            out = "Coge dos cartas de recursos de la reserva.";
            break;
        case 3:
            out = "¡Has ganado un punto de victoria!";
            break;
        case 4:
            out = "Todos los oponentes deberán entregarte de su mano las cartas del recurso que elijas.";
            break;
        default:
            out = "Texto desconocido";
            break;
    }
    return out;
}

function getExchangeButtonText(_num) {
    var out = "";
    switch (parseInt(_num)) {
        case 1:
            out = "4 cartas de";
            break;
        case 2:
            out = "3 cartas de";
            break;
        // case 3:
        //     out = "2 cartas de";
        //     break;
        // case 4:
        //     out = "2 cartas de";
        //     break;
        // case 5:
        //     out = "2 cartas de";
        //     break;
        // case 6:
        //     out = "2 cartas de";
        //     break;
        // case 7:
        //     out = "2 cartas de";
        //     break;
        default:
            // out = "Texto desconocido";
            out = "2 cartas de";
            break;
    }
    return out;
}
