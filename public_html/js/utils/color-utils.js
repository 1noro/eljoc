
function getColorStr(_num) {
    var out = "";
    switch (parseInt(_num)) {
        case 0:
            out = "Desconocido";
            break;
        case 1:
            out = "Amarillo";
            break;
        case 2:
            out = "Azul";
            break;
        case 3:
            out = "Morado";
            break;
        case 4:
            out = "Rojo";
            break;
        default:
            out = "Desconocido";
            break;
    }
    return out;
}
