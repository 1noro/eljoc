// cursor_utils.js
// (c) inoro 2020 GPL v3

function getCanvasStartPoint() {
    var bc_satrt_point_x = (window.innerWidth - window.joc.dbw) / 2;
    var bc_satrt_point_y = window.joc.canvasMarginTop;
    return {x : bc_satrt_point_x, y : bc_satrt_point_y};
}

// function for testing
function getCursor(e) {
    var cursorX = (window.Event) ? e.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
    var cursorY = (window.Event) ? e.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    console.log('[CLIC] x: ' + cursorX + ', y: ' + cursorY);
}

function getCursolRelAndDoSomething(e) {
    if (window.gui.isBoardClickable()) {
        var cursorX = (window.Event) ? e.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        var cursorY = (window.Event) ? e.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
        // console.log('[CLIC] rel_x: '+(cursorX-getCanvasStartPoint().x)+', rel_y: '+(cursorY-getCanvasStartPoint().y));

        cursorX_rel = cursorX - getCanvasStartPoint().x;
        cursorY_rel = cursorY - getCanvasStartPoint().y;
        // console.log('[CLIC] ' + cursorX_rel + ', ' + cursorY_rel + ' (cursorX_rel, cursorY_rel)');

        //cursorX_rel += 2; // correccíon de desfase que no entiendo de donde viene

        cursorX_relM = (cursorX_rel / window.joc.dm) * window.joc.m;
        cursorY_relM = (cursorY_rel / window.joc.dm) * window.joc.m;
        console.log('[CLIC] ' + cursorX_relM + ', ' + cursorY_relM + ' (cursorX_relM, cursorY_relM)');

        // aplicamos el click a los objetos del tablero
        window.g.getClickBehavior().parser(
            cursorX_relM,
            cursorY_relM,
            window.g.getData().game.phase,
            window.g.getData().game.round
        );
    } else {
        // console.error("[FAIL] no permitido el click sobre el tablero hasta que te unas a una partida");
    }
}
