
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function checkAndGetParamGET(_parameterName) {
    var out = 0;
    var valueGET = findGetParameter(_parameterName);
    if (isDefinedNumber(valueGET)) {
        out = parseInt(valueGET);
        console.log("[ SET] " + _parameterName + " = " + out);
    }
    return out;
}
