
class TestClass {
    verifyed = false;
    constructor() {
        this.verifyed = true;
    }
    getVerifyed() {
        return this.verifyed;
    }
}

function getHTMLElements() {
    window.joc.generalContDiv = document.getElementById('general_cont');
    window.joc.topMsgCompatibilityDiv = document.getElementById('top_msg_compatibility');
    window.joc.loadingScreen = document.getElementById('loading_screen');
    window.joc.boardCanvasContDiv = document.getElementById('board_canvas_cont');

    window.joc.boardCanvas = document.getElementById('board_canvas');
    window.joc.ctx = window.joc.boardCanvas.getContext("2d");
}

function checkCompatibility() {
    var testObj = new TestClass();
    if (testObj.getVerifyed()) {
        // Si hemos llegado hasta aquí sin errores podemos quitar el mensaje de compatibilidad
        window.joc.topMsgCompatibilityDiv.parentNode.removeChild(window.joc.topMsgCompatibilityDiv);
        // window.joc.loadingScreen.parentNode.removeChild(window.joc.loadingScreen);
        // window.joc.loadingScreen.style.display = "none";
        console.log("[INFO] eliminado el mensaje de compatibilidad");
    }
}
