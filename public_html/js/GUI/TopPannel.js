
class TopPannel {

    topPannelContDiv;
    topMsgTitleDiv;

    constructor() {
        this.topPannelContDiv = document.getElementById("top_pannel");
        this.topMsgTitleDiv = this.generateTopMsgTitleDiv();
        this.draw();
    }

    generateTopMsgTitleDiv() {
        var topMsgTitleDiv = document.createElement("div");
        topMsgTitleDiv.id = "top_msg_title";
        topMsgTitleDiv.className = "item title";
        topMsgTitleDiv.innerHTML = "&nbsp;";
        return topMsgTitleDiv;
    }

    draw() {
        this.topPannelContDiv.appendChild(this.topMsgTitleDiv);
    }

    showTopMsg(_txt) {
        this.topMsgTitleDiv.innerHTML = "";
        this.topMsgTitleDiv.innerText = _txt;
    }

    cleanTopMsg() {
        this.topMsgTitleDiv.innerHTML = "&nbsp;";
    }
}
