
class MiddlePannel {

    middlePannelContDiv;
    waitingGifContDiv;

    loginForm;

    constructor() {
        this.middlePannelContDiv = document.getElementById('middle_pannel');
        this.waitingGifContDiv = this.generateWaitingGifContDiv();
        this.loginForm = new LoginForm();
        this.draw();
    }

    generateWaitingGifContDiv() {
        var waitingGifContDiv = document.createElement("div");
        waitingGifContDiv.id = "waiting_gif_cont";
        waitingGifContDiv.className = "item";

        var waitingGif = document.createElement("img");
        waitingGif.src = "img/hexGIF.gif";
        waitingGif.alt = "waiting_gif";

        waitingGifContDiv.appendChild(waitingGif);

        return waitingGifContDiv;
    }

    draw() {
        var loginFormContDiv = this.loginForm.getContDiv();
        this.middlePannelContDiv.style.display = "none";
        loginFormContDiv.style.display = "none";
        this.middlePannelContDiv.appendChild(loginFormContDiv);
        this.middlePannelContDiv.appendChild(this.waitingGifContDiv);
        this.middlePannelContDiv.style.display = "block";
    }

    getLoginForm() {
        return this.loginForm;
    }

    showWaitingGif() {
        this.waitingGifContDiv.style.display = "block";
    }

    hideWaitingGif() {
        this.waitingGifContDiv.style.display = "none";
    }

    showLoginForm(_msg) {
        this.loginForm.show(_msg);
    }

    hideLoginForm() {
        this.loginForm.hide();
    }

    show() {
        this.middlePannelContDiv.style.display = "block";
    }

    hide() {
        this.middlePannelContDiv.style.display = "none";
        this.waitingGifContDiv.style.display = "none";
        this.loginForm.hide();
    }

}
