
class OpponentItem {

    id;
    color;
    online;
    ready;
    turn;

    pannelDivCont;
    pannelItemDiv;

    hand = 0;
    developmentHidden = 0;
    knightsShown = 0;

    knights = false;
    roads = false;

    displayReady;

    constructor(_id, _color, _online, _ready, _displayReady, _turn, _pannelDivCont) {
        this.id = _id;
        this.color = _color;
        this.online = _online;
        this.ready = _ready;
        this.displayReady = _displayReady;
        this.turn = _turn;
        this.pannelDivCont = _pannelDivCont;

        this.pannelItemDiv = this.generatePannelItemDiv();
    }

    getId() {
        return this.id;
    }

    setId(_id) {
        this.id = _id;
    }

    setOnline(_online) {
        this.online = _online;
    }

    setReady(_ready) {
        this.ready = _ready;
    }

    setDisplayReady(_displayReady) {
        this.displayReady = _displayReady;
    }

    setTurn(_turn) {
        this.turn = _turn;
    }

    generatePannelItemDiv() {

        // <div id="opponents_0" class="item">
        //     <div class="avatar color0">
        //         <img alt="player_red" src="img/AvatarRojo.png" title="Jugador Rojo">
        //     </div>
        //     <div class="online on">&nbsp;</div>
        //     <div class="attributes_cont">
        //         <div id="attr0" class="attr" title="Cartas en mano">0</div>
        //         <div id="attr1" class="attr" title="Cartas de desarrollo sin usar">0</div>
        //         <div id="attr2" class="attr" title="Caballeros levantados">0</div>
        //     </div>
        //     <div class="roads">CAMIN.</div>
        //     <div class="knights">CABALL.</div>
        // </div>

        var itemDiv = document.createElement("div");
        itemDiv.id = "opponent" + this.color;
        itemDiv.className = "item";

        var avatarDiv = document.createElement("div");
        avatarDiv.className = "avatar color" + this.color;

        var avatarImg = document.createElement("img");
        avatarImg.alt = "Jugador " + getColorStr(this.color) + " (" + this.id + ")";
        avatarImg.title = "Jugador " + getColorStr(this.color) + " (" + this.id + ")";
        avatarImg.src = "img/avatar" + this.color + "_80.png";

        avatarDiv.appendChild(avatarImg);

        var onlineDiv = document.createElement("div");
        onlineDiv.className = "online " + this.getOnlineStr();
        onlineDiv.innerHTML = "&nbsp;";

        var attributesContDiv = document.createElement("div");
        attributesContDiv.className = "attributes_cont";

        var attr0Div = document.createElement("div");
        attr0Div.id = "attr0";
        attr0Div.className = "attr";
        attr0Div.title = "Cartas en mano";
        attr0Div.innerText = 0;
        attributesContDiv.appendChild(attr0Div);

        var attr1Div = document.createElement("div");
        attr1Div.id = "attr1";
        attr1Div.className = "attr";
        attr1Div.title = "Cartas de desarrollo sin usar";
        attr1Div.innerText = 0;
        attributesContDiv.appendChild(attr1Div);

        var attr2Div = document.createElement("div");
        attr2Div.id = "attr2";
        attr2Div.className = "attr";
        attr2Div.title = "Caballeros levantados";
        attr2Div.innerText = 0;
        attributesContDiv.appendChild(attr2Div);

        var roadsDiv = document.createElement("div");
        roadsDiv.className = "card_icon roads";
        roadsDiv.innerHTML = "&nbsp;";
        roadsDiv.title = "Mayor ruta comercial";

        var knightsDiv = document.createElement("div");
        knightsDiv.className = "card_icon knights";
        knightsDiv.innerHTML = "&nbsp;";
        knightsDiv.title = "Mayor ejército";

        var readyDiv = document.createElement("div");
        readyDiv.className = "card_icon ready";
        readyDiv.innerHTML = "&nbsp;";
        readyDiv.title = "El jugador está listo";

        itemDiv.appendChild(avatarDiv);
        itemDiv.appendChild(onlineDiv);
        itemDiv.appendChild(attributesContDiv);
        itemDiv.appendChild(roadsDiv);
        itemDiv.appendChild(knightsDiv);
        itemDiv.appendChild(readyDiv);

        return itemDiv;
    }

    drawPannelDiv() {
        // console.log(this.pannelItemDiv);
        if (this.displayReady && parseInt(this.ready)) this.pannelItemDiv.children[5].style.display = "block";
        if (parseInt(this.turn) == 1) this.pannelItemDiv.children[0].className = "avatar color0";
        this.pannelDivCont.appendChild(this.pannelItemDiv);
    }

    getOnlineStr() {
        var out = "";
        switch (parseInt(this.online)) {
            case 0:
                out = "off";
                break;
            case 1:
                out = "on";
                break;
            default:
                out = "off";
                break;
        }
        return out;
    }
}
