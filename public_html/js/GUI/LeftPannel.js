
class LeftPannel {

    leftPannelContDiv;
    resourcesPannelContDiv;
    developmentPannelContDiv;

    resNumberContDiv;
    resNumberDiv;
    devNumberContDiv;
    devNumberDiv;

    switchButtonContDiv;
    switchButtonDiv;

    resourceCardArr;
    developmentCardArr;

    numberOfResourceCards = 0;
    numberOfDevelopmentCards = 0;

    leftPannelState = 0; // 0 = showResourcePannel; 1 = showDevelopmentPannel

    drawed = false;

    constructor() {
        this.leftPannelContDiv = document.getElementById("left_pannel");
        this.resourcesPannelContDiv = this.generateResourcesPannelContDiv();
        this.developmentPannelContDiv = this.generateDevelopmentPannelContDiv();
        this.resNumberContDiv = this.generateResNumberContDiv();
        this.resNumberDiv = this.generateResNumberDiv();
        this.devNumberContDiv = this.generateDevNumberContDiv();
        this.devNumberDiv = this.generateDevNumberDiv();
        this.switchButtonContDiv = this.generateSwitchButtonContDiv();
        this.switchButtonDiv = this.generateSwitchButtonDiv();
    }

    generateResourcesPannelContDiv() {
        var resourcesPannelContDiv = document.createElement("div");
        resourcesPannelContDiv.id = "resources_pannel";
        return resourcesPannelContDiv;
    }

    generateDevelopmentPannelContDiv() {
        var developmentPannelContDiv = document.createElement("div");
        developmentPannelContDiv.id = "development_pannel";
        developmentPannelContDiv.style.display = "none";
        return developmentPannelContDiv;
    }

    generateResNumberContDiv() {
        var resNumberContDiv = document.createElement("div");
        resNumberContDiv.id = "res_number_cont";
        resNumberContDiv.className = "item";
        resNumberContDiv.title = "Cartas de recursos en tu mano";
        return resNumberContDiv;
    }

    generateResNumberDiv() {
        var resNumberDiv = document.createElement("div");
        resNumberDiv.id = "res_number";
        resNumberDiv.innerText = "0";
        return resNumberDiv;
    }

    generateDevNumberContDiv() {
        var devNumberContDiv = document.createElement("div");
        devNumberContDiv.id = "dev_number_cont";
        devNumberContDiv.className = "item";
        devNumberContDiv.title = "Cartas de desarrollo en tu mano";
        return devNumberContDiv;
    }

    generateDevNumberDiv() {
        var devNumberDiv = document.createElement("div");
        devNumberDiv.id = "dev_number";
        devNumberDiv.innerText = "0";
        return devNumberDiv;
    }

    generateSwitchButtonContDiv() {
        var switchButtonContDiv = document.createElement("div");
        switchButtonContDiv.id = "switch_button_cont";
        switchButtonContDiv.className = "item";
        switchButtonContDiv.title = "Cambiar para ver cartas de desarrollo";

        switchButtonContDiv.onclick = function() {
            window.gui.switchLeftPannelState();
        };

        return switchButtonContDiv;
    }

    generateSwitchButtonDiv() {
        var switchButtonDiv = document.createElement("div");
        switchButtonDiv.id = "switch_button";
        return switchButtonDiv;
    }

    // -------------------------------------------------------------------------

    draw() {
        this.leftPannelContDiv.style.display = "none";

        this.resNumberContDiv.appendChild(this.resNumberDiv);
        this.resourcesPannelContDiv.appendChild(this.resNumberContDiv);

        this.devNumberContDiv.appendChild(this.devNumberDiv);
        this.developmentPannelContDiv.appendChild(this.devNumberContDiv);

        this.resourceCardArr = new Array();
        for (var i = 0; i < 5; i++) {
            this.resourceCardArr[i] = new ResourceCardItem(i + 1);
            this.resourcesPannelContDiv.appendChild(this.resourceCardArr[i].getContDiv());
        }

        this.developmentCardArr = new Array();
        for (var i = 0; i < 5; i++) {
            this.developmentCardArr[i] = new DevelopmentCardItem(i);
            this.developmentPannelContDiv.appendChild(this.developmentCardArr[i].getContDiv());
        }

        this.leftPannelContDiv.appendChild(this.resourcesPannelContDiv);
        this.leftPannelContDiv.appendChild(this.developmentPannelContDiv);

        this.switchButtonContDiv.appendChild(this.switchButtonDiv);
        this.leftPannelContDiv.appendChild(this.switchButtonContDiv);

        this.drawed = true;
        this.leftPannelContDiv.style.display = "block";
    }

    hide() {
        this.leftPannelContDiv.style.display = "none";
    }

    show() {
        if (!this.drawed) this.draw();
        this.leftPannelContDiv.style.display = "block";
    }

    showResourcePannel() {
        this.developmentPannelContDiv.style.display = "none";
        this.resourcesPannelContDiv.style.display = "block";
        this.switchButtonContDiv.title = "Cambiar para ver cartas de desarrollo";
    }

    showDevelopmentPannel() {
        this.resourcesPannelContDiv.style.display = "none";
        this.developmentPannelContDiv.style.display = "block";
        this.switchButtonContDiv.title = "Cambiar para ver cartas de recursos";
    }

    switchState() {
        if (this.leftPannelState == 0) {
            this.leftPannelState = 1;
            this.showDevelopmentPannel();
        } else {
            this.leftPannelState = 0;
            this.showResourcePannel();
        }
    }

    // -------------------------------------------------------------------------

    addResourceCard(resourceId, number2add) {
        this.resourceCardArr[resourceId - 1].addCards(number2add);
        this.numberOfResourceCards += number2add;
        this.resNumberDiv.innerText = this.numberOfResourceCards;
        if (this.numberOfResourceCards > 7) {
            this.resNumberDiv.className = "red";
        }
    }

    removeResourceCard(resourceId, number2remove) {
        this.resourceCardArr[resourceId - 1].removeCards(number2remove);
        this.numberOfResourceCards -= number2remove;
        this.resNumberDiv.innerText = this.numberOfResourceCards;
        if (this.numberOfResourceCards > 7) {
            this.resNumberDiv.className = "red";
        } else {
            this.resNumberDiv.className = "";
        }
    }

    addDevelopmentCard(developmentId, number2add) {
        this.developmentCardArr[developmentId].addCards(number2add);
        this.numberOfDevelopmentCards += number2add;
        this.devNumberDiv.innerText = this.numberOfDevelopmentCards;
    }

    removeDevelopmentCard(developmentId, number2remove) {
        this.developmentCardArr[developmentId].removeCards(number2remove);
        this.numberOfDevelopmentCards -= number2remove;
        this.devNumberDiv.innerText = this.numberOfDevelopmentCards;
    }

}
