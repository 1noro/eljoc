
class RightPannel {

    rightPannelContDiv;

    menuButtonDiv;

    opponentsPannelContDiv;
    opponentArr = new Array(); // lista de los oponentes

    gameInfoContDiv;

    constructor() {
        this.rightPannelContDiv = document.getElementById("right_pannel");
        this.menuButtonDiv = this.generateMenuButtonDiv();
        // this.opponentsPannelContDiv = document.getElementById('opponents_pannel');
        this.opponentsPannelContDiv = this.generateOpponentsPannelContDiv();
        // this.gameInfoContDiv = document.getElementById('game_info_cont');
        this.gameInfoContDiv = this.generateGameInfoContDiv();
        this.draw();
    }

    generateMenuButtonDiv() {
        var menuButtonDiv = document.createElement("div");
        menuButtonDiv.id = "menu_button";
        menuButtonDiv.className = "item";
        menuButtonDiv.title = "Menú";
        return menuButtonDiv;
    }

    generateOpponentsPannelContDiv() {
        var opponentsPannelContDiv = document.createElement("div");
        opponentsPannelContDiv.id = "opponents_pannel";
        return opponentsPannelContDiv;
    }

    generateGameInfoContDiv() {
        var gameInfoContDiv = document.createElement("div");
        gameInfoContDiv.id = "game_info_cont";
        gameInfoContDiv.className = "item";

        var textLeft = document.createElement("div");
        textLeft.className = "text left";

        gameInfoContDiv.appendChild(textLeft);

        return gameInfoContDiv;
    }

    draw() {
        this.rightPannelContDiv.style.display = "none";
        this.rightPannelContDiv.appendChild(this.menuButtonDiv);
        this.rightPannelContDiv.appendChild(this.opponentsPannelContDiv);
        this.rightPannelContDiv.appendChild(this.gameInfoContDiv);
        this.rightPannelContDiv.style.display = "block";
    }

    // -------------------------------------------------------------------------

    createOpponentItems(opponents, waiting) {
        for (var i = 0; i < opponents.length; i++) {
            this.opponentArr[i] = new OpponentItem(
                opponents[i].id,
                opponents[i].color,
                opponents[i].online,
                opponents[i].ready,
                parseInt(waiting) == 1 ? true : false,
                opponents[i].turn,
                this.opponentsPannelContDiv
            );
        }
    }

    showOpponentItems() {
        for (var i = 0; i < this.opponentArr.length; i++) {
            this.opponentArr[i].drawPannelDiv();
        }
    }

    refreshOpponentItems(opponents, waiting) {
        this.opponentsPannelContDiv.innerHTML = "";
        this.createOpponentItems(opponents, waiting);
        this.showOpponentItems();
    }

    // -------------------------------------------------------------------------

    displayGameInfo(player_id, player_color, game_id) {
        this.gameInfoContDiv.style.display = "block";
        this.gameInfoContDiv.children[0].innerHTML = "\
            <p><center>Tu info</center></p>\
            player: " + player_id + "<br>\
            game: " + game_id + "<br>\
            color: " + player_color + "<br>\
            (testing on)\
        ";
    }

}
