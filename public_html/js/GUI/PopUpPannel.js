
class PopUpPannel {

    popUpPannelContDiv;

    popUpCloseBgDiv;
    popUpContDiv;
    popUpTitleDiv;
    popUpCloseDiv;
    bankExchangeMenu;

    allowSimpleClose = false;

    constructor() {
        this.popUpPannelContDiv = document.getElementById("popup_pannel");
        this.popUpCloseBgDiv = this.generatePopUpCloseBgDiv();
        this.popUpContDiv = this.generatePopUpContDiv();
        this.popUpTitleDiv = this.generatePopUpTitleDiv();
        this.popUpCloseDiv = this.generatePopUpCloseDiv();
        this.bankExchangeMenu = new BankExchangeMenu(this.popUpContDiv);

        this.draw();
        // this.showWonResourceCards([1, 3, 1, 0, 1]);
        // this.showWonDevelopmentCard(4);
        // this.showOpponentsToSteal([1, 2, 3]);
        this.showBankExchangeMenu([0, 0, 4, 0, 4], [1, 1, 1, 1, 1, 1]);
    }

    generatePopUpCloseBgDiv() {
        var popUpCloseBgDiv = document.createElement("div");
        popUpCloseBgDiv.id = "close_bg";

        popUpCloseBgDiv.onclick = function() {
            window.gui.simpleClosePopUp();
        };

        return popUpCloseBgDiv;
    }

    generatePopUpContDiv() {
        var popUpContDiv = document.createElement("div");
        popUpContDiv.id = "popup_cont";
        popUpContDiv.className = "pannel center";
        return popUpContDiv;
    }

    generatePopUpTitleDiv() {
        var popUpTitleDiv = document.createElement("div");
        popUpTitleDiv.id = "popup_title";
        popUpTitleDiv.className = "item";
        popUpTitleDiv.innerHTML = "&nbsp;";
        return popUpTitleDiv;
    }

    generatePopUpCloseDiv() {
        var popUpCloseDiv = document.createElement("div");
        popUpCloseDiv.id = "close";

        popUpCloseDiv.onclick = function() {
            window.gui.closePopUp();
        };

        return popUpCloseDiv;
    }

    draw() {
        this.popUpPannelContDiv.appendChild(this.popUpCloseBgDiv);
        this.popUpPannelContDiv.appendChild(this.popUpContDiv);
    }

    appendTitle(msg) {
        this.popUpTitleDiv.innerText = msg;
        this.popUpContDiv.appendChild(this.popUpTitleDiv);
    }

    appendClose() {
        this.popUpContDiv.appendChild(this.popUpCloseDiv);
    }

    show() {
        this.popUpPannelContDiv.style.display = "block";
    }

    hide() {
        this.popUpPannelContDiv.style.display = "none";
    }

    resetPopUpContDiv() {
        this.popUpContDiv.innerHTML = "";
    }

    simpleClose() {
        if (this.allowSimpleClose) this.hide();
    }

    close() {
        this.hide();
    }

    // -------------------------------------------------------------------------

    generateWonCardDescContDiv(txt) {
        var wonCardDescContDiv = document.createElement("div");
        wonCardDescContDiv.id = "won_card_description_cont";
        wonCardDescContDiv.innerText = txt;
        return wonCardDescContDiv;
    }

    // -------------------------------------------------------------------------

    // wonCardsArr = [1, 0, 3, 0, 0] --> Ha ganado 1 carta de trigo y 3 de oveja
    showWonResourceCards(wonCardsArr) {
        this.hide();
        this.allowSimpleClose = true;
        this.resetPopUpContDiv();
        this.appendTitle("Has ganado las siguientes cartas");

        var wonCardsContDiv = this.generateItem("won_cards_cont");
        for (var i = 0; i < wonCardsArr.length; i++) {
            if (wonCardsArr[i] > 0) {
                var wonCardImg = document.createElement("img");
                wonCardImg.id = "won_card_res" + (i + 1);
                wonCardImg.className = "won_card";
                wonCardImg.src = "img/res" + (i + 1) + "_150.png";
                wonCardsContDiv.appendChild(wonCardImg);
            }
        }
        this.popUpContDiv.appendChild(wonCardsContDiv);

        var wonCardsNumContDiv = this.generateItem("won_cards_num_cont");
        for (var i = 0; i < wonCardsArr.length; i++) {
            if (wonCardsArr[i] > 0) {
                var wonCardNum = document.createElement("span");
                wonCardNum.id = "won_card_res" + (i + 1) + "_num";
                wonCardNum.className = "won_card_num";
                wonCardNum.innerText = "+" + wonCardsArr[i];
                wonCardsNumContDiv.appendChild(wonCardNum);
            }
        }
        this.popUpContDiv.appendChild(wonCardsNumContDiv);

        this.appendClose();
        this.show();
    }

    showWonDevelopmentCard(developmentId) {
        this.hide();
        this.allowSimpleClose = true;
        this.resetPopUpContDiv();
        this.appendTitle("Has obtenido una carta de " + getDevelopmentStr(developmentId));

        var wonCardsContDiv = this.generateItem("won_cards_cont");
        var wonCardImg = document.createElement("img");
        wonCardImg.id = "won_card_dev" + developmentId;
        wonCardImg.className = "won_card";
        wonCardImg.src = "img/dev" + developmentId + "_150.png";
        wonCardsContDiv.appendChild(wonCardImg);
        this.popUpContDiv.appendChild(wonCardsContDiv);

        var wonCardDescContDiv = this.generateWonCardDescContDiv(getDevelopmentDesc(developmentId));
        this.popUpContDiv.appendChild(wonCardDescContDiv);

        this.appendClose();
        this.show();
    }

    showOpponentsToSteal(colorsArr) {
        this.hide();
        this.allowSimpleClose = true;
        this.resetPopUpContDiv();
        this.appendTitle("Elige un oponente para robarle una carta");

        var opponentsContDiv = this.generateItem("opponents_cont");
        for (var i = 0; i < colorsArr.length; i++) {
            var avatarDiv = generateDiv("avatar" + colorsArr[i], "avatar");
            var avatarImg = document.createElement("img");
            avatarImg.alt = "Jugador " + getColorStr(colorsArr[i]);
            avatarImg.title = "Jugador " + getColorStr(colorsArr[i]);
            avatarImg.src = "img/avatar" + colorsArr[i] + "_150.png";

            avatarDiv.appendChild(avatarImg);
            opponentsContDiv.appendChild(avatarDiv);
        }
        this.popUpContDiv.appendChild(opponentsContDiv);

        this.appendClose();
        this.show();
    }

    // playerResCardsArr[5] - array con el númer de cartas de cada uno de los 5 tipos
    // playerHarborArr[6] - con los tipos de puerto que pertenecen al jugador
    showBankExchangeMenu(playerResCardsArr, playerHarborArr) {
        this.hide();
        this.resetPopUpContDiv();
        this.appendTitle("Intercambia recursos con la reserva");

        this.bankExchangeMenu.draw(playerResCardsArr, playerHarborArr);

        this.appendClose();
        this.show();
    }

    selectLeftButton(id) {
        this.bankExchangeMenu.selectLeftButton(id);
    }

    selectRightResource(id) {
        this.bankExchangeMenu.selectRightResource(id);
    }

    next4xResource() {
        this.bankExchangeMenu.next4xResource();
    }

    prev4xResource() {
        this.bankExchangeMenu.prev4xResource();
    }

    next3xResource() {
        this.bankExchangeMenu.next3xResource();
    }

    prev3xResource() {
        this.bankExchangeMenu.prev3xResource();
    }
}
