
class BottomPannel {

    bottomPannelContDiv;

    waitBarContDiv;
    readyButtonContDiv;
    dicesContDiv;

    visible = "";

    constructor() {
        this.bottomPannelContDiv = document.getElementById('bottom_pannel');
    }

    setVisible(_visible) {
        this.visible = _visible;
    }

    hide() {
        if (this.visible != "none") {
            this.setVisible("none");
            this.bottomPannelContDiv.innerHTML = "";
        }
    }

    displayWaitBar() {
        if (this.visible != "wait_bar") {
            this.setVisible("wait_bar");
            // <div id="wait_bar_cont" class="item" title="Esperando...">
            //     <img alt="barra_de_espera" src="img/espera.gif">
            // </div>

            var waitBarContDiv = document.createElement("div");
            waitBarContDiv.id = "wait_bar_cont";
            waitBarContDiv.className = "item";
            waitBarContDiv.title = "Esperando...";
            waitBarContDiv.style.display = "none";

            var waitBarImg = document.createElement("img");
            waitBarImg.alt = "wait_bar_img";
            waitBarImg.src = "img/espera.gif";

            waitBarContDiv.appendChild(waitBarImg);
            this.waitBarContDiv = waitBarContDiv;
            this.bottomPannelContDiv.innerHTML = "";
            this.bottomPannelContDiv.appendChild(this.waitBarContDiv);
            this.waitBarContDiv.style.display = "block";
        }
    }

    displayReadyButton() {
        if (this.visible != "ready_button") {
            this.setVisible("ready_button");

            var readyButtonContDiv = document.createElement("div");
            readyButtonContDiv.id = "ready_button_cont";
            readyButtonContDiv.className = "item";
            readyButtonContDiv.title = "Pulsa aquí si estás listo para jugar";
            readyButtonContDiv.style.display = "none";

            var readyButtonDiv = document.createElement("div");
            readyButtonDiv.className = "button";
            readyButtonDiv.innerText = "Listo";

            readyButtonDiv.onclick = function() {
                readFromPHP("set_ready", window.g.getDataJSON());
            };

            readyButtonContDiv.appendChild(readyButtonDiv);
            this.readyButtonContDiv = readyButtonContDiv;
            this.bottomPannelContDiv.innerHTML = "";
            this.bottomPannelContDiv.appendChild(this.readyButtonContDiv);
            this.readyButtonContDiv.style.display = "block";
        }
    }

    displayDices() {
        if (this.visible != "dices") {
            this.setVisible("dices");

            var dicesContDiv = document.createElement("div");
            dicesContDiv.id = "dices_cont";
            dicesContDiv.className = "item";

            dicesContDiv.onclick = function() {
                if (window.g.getClickBehavior().getDicesAllowClick()) {
                    window.g.getClickBehavior().setDicesAllowClick(false);
                    var valueDice1 = Math.floor(Math.random() * 6) + 1;
                    var valueDice2 = Math.floor(Math.random() * 6) + 1;
                    window.g.setDicesValue(valueDice1, valueDice2);

                    if (window.g.getData().game.phase == 2) {
                        if (window.g.detectEndOfRound()) {
                            window.g.endPhase2("turn_dice_value", valueDice1 + valueDice2);
                        } else {
                            window.g.registerTmpData("turn_dice_value", valueDice1 + valueDice2);
                            window.g.passTurn();
                        }
                    }

                    this.style.cursor = "default";
                    this.children[0].innerText = valueDice1;
                    this.children[1].innerText = valueDice1 + valueDice2;
                    this.children[2].innerText = valueDice2;
                }
            };

            var leftDice = document.createElement("div");
            leftDice.className = "dice left";
            leftDice.innerText = "?";

            var rightDice = document.createElement("div");
            rightDice.className = "dice right";
            rightDice.innerText = "?";

            var centerText = document.createElement("div");
            centerText.className = "center_text";
            centerText.innerText = "¡TIRA!";

            dicesContDiv.appendChild(leftDice);
            dicesContDiv.appendChild(centerText);
            dicesContDiv.appendChild(rightDice);
            this.dicesContDiv = dicesContDiv;
            this.bottomPannelContDiv.innerHTML = "";
            this.bottomPannelContDiv.appendChild(this.dicesContDiv);
            this.dicesContDiv.style.display = "block";
        }
    }
}
