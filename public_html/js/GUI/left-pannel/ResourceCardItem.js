
class ResourceCardItem {

    resourceId;

    resourceCardContDiv;
    resourceCardImg;
    resourceCardNumberDiv;

    numberOfCards = 0;
    resurceImgSRC = "";
    backResurceSRC = "img/backRes_150.png";

    constructor(_resourceId) {
        this.resourceId = _resourceId;
        this.resurceImgSRC = "img/res" + _resourceId + "_150.png";

        this.resourceCardContDiv = this.generateResourceCardContDiv();
        this.resourceCardImg = this.generateResourceCardImg();
        this.resourceCardNumberDiv = this.generateResourceCardNumberDiv();

        this.resourceCardContDiv.appendChild(this.resourceCardImg);
        this.resourceCardContDiv.appendChild(this.resourceCardNumberDiv);
    }

    generateResourceCardContDiv() {
        var resourceCardContDiv = document.createElement("div");
        resourceCardContDiv.id = "card_res" + this.resourceId;
        resourceCardContDiv.className = "item";
        return resourceCardContDiv;
    }

    generateResourceCardImg() {
        var resourceCardImg = document.createElement("img");
        resourceCardImg.alt = "card_res" + this.resourceId;
        resourceCardImg.title = getResourceStr(this.resourceId);
        resourceCardImg.src = this.backResurceSRC;
        return resourceCardImg;
    }

    generateResourceCardNumberDiv() {
        var resourceCardNumberDiv = document.createElement("div");
        resourceCardNumberDiv.id = "card_res" + this.resourceId + "_num";
        resourceCardNumberDiv.className = "number";
        resourceCardNumberDiv.innerText = this.numberOfCards;
        return resourceCardNumberDiv;
    }

    // -------------------------------------------------------------------------

    getResourceCardContDiv() {
        return this.resourceCardContDiv;
    }

    getContDiv() {
        return this.resourceCardContDiv;
    }

    // -------------------------------------------------------------------------

    addCards(number2add) {
        this.numberOfCards += number2add;
        this.resourceCardNumberDiv.innerText = this.numberOfCards;
        if (this.numberOfCards > 0) {
            this.resourceCardImg.src = this.resurceImgSRC;
        }
    }

    removeCards(number2remove) {
        this.numberOfCards -= number2remove;
        this.resourceCardNumberDiv.innerText = this.numberOfCards;
        if (this.numberOfCards > 0) {
            this.resourceCardImg.src = this.resurceImgSRC;
        } else {
            this.resourceCardImg.src = this.backResurceSRC;
        }
    }

}
