
class DevelopmentCardItem {

    developmentId;

    developmentCardContDiv;
    developmentCardImg;
    developmentCardNumberDiv;

    numberOfCards = 0;
    developmentImgSRC = "";
    backDevelopmentSRC = "img/backDev_150.png";

    constructor(_developmentId) {
        this.developmentId = _developmentId;
        this.developmentImgSRC = "img/dev" + _developmentId + "_150.png";

        this.developmentCardContDiv = this.generateDevelopmentCardContDiv();
        this.developmentCardImg = this.generateDevelopmentCardImg();
        this.developmentCardNumberDiv = this.generateDevelopmentCardNumberDiv();

        this.developmentCardContDiv.appendChild(this.developmentCardImg);
        this.developmentCardContDiv.appendChild(this.developmentCardNumberDiv);
    }

    generateDevelopmentCardContDiv() {
        var developmentCardContDiv = document.createElement("div");
        developmentCardContDiv.id = "card_dev" + this.developmentId;
        developmentCardContDiv.className = "item dev";
        return developmentCardContDiv;
    }

    generateDevelopmentCardImg() {
        var developmentCardImg = document.createElement("img");
        developmentCardImg.alt = "card_dev" + this.developmentId;
        developmentCardImg.title = getDevelopmentStr(this.developmentId);
        developmentCardImg.src = this.backDevelopmentSRC;
        return developmentCardImg;
    }

    generateDevelopmentCardNumberDiv() {
        var developmentCardNumberDiv = document.createElement("div");
        developmentCardNumberDiv.id = "card_res" + this.developmentId + "_num";
        developmentCardNumberDiv.className = "number";
        developmentCardNumberDiv.innerText = this.numberOfCards;
        return developmentCardNumberDiv;
    }

    // -------------------------------------------------------------------------

    getDevelopmentCardContDiv() {
        return this.developmentCardContDiv;
    }

    getContDiv() {
        return this.developmentCardContDiv;
    }

    // -------------------------------------------------------------------------

    addCards(number2add) {
        this.numberOfCards += number2add;
        this.developmentCardNumberDiv.innerText = this.numberOfCards;
        if (this.numberOfCards > 0) {
            this.developmentCardImg.src = this.developmentImgSRC;
        }
    }

    removeCards(number2remove) {
        this.numberOfCards -= number2remove;
        this.developmentCardNumberDiv.innerText = this.numberOfCards;
        if (this.numberOfCards > 0) {
            this.developmentCardImg.src = this.developmentImgSRC;
        } else {
            this.developmentCardImg.src = this.backDevelopmentSRC;
        }
    }

}
