
class BankExchangeMenu {

    popUpContDiv;

    playerResCardsArr;
    playerHarborArr;

    exchangeLeftContDiv;
    exchangeRightContDiv;
    leftButtonArr; // siempre tiene la misma estructura (7 posiciones), y si algún botón no se muestra se pone a 0
    leftButtonSelected = 0;
    rightResourceSelected = 0;
    leftButton4xResId = 1;
    leftButton4xResIcon;
    leftButton3xResId = 1;
    leftButton3xResIcon;

    constructor(_popUpContDiv) {
        this.popUpContDiv = _popUpContDiv;
    }

    // playerResCardsArr[5] - array con el númer de cartas de cada uno de los 5 tipos
    // playerHarborArr[6] - con los tipos de puerto que pertenecen al jugador
    draw(playerResCardsArr, playerHarborArr) {
        this.playerResCardsArr = playerResCardsArr;
        this.playerHarborArr = playerHarborArr;

        var exchangeContDiv = generateItem("exchange_cont");
        var exchangeLeftContDiv = generateDiv("exchange_left", "exchange_side");
        exchangeLeftContDiv.appendChild(this.generateExchangeLeftTitleDiv());
        var exchangeRightContDiv = generateDiv("exchange_right", "exchange_side");
        exchangeRightContDiv.appendChild(this.generateExchangeRightTitleDiv());

        // --- botones de la izquierda -----------------------------------------
        this.leftButtonArr = new Array();
        for (var i = 1; i <= 7; i++) {
            if (i == 1 || (i > 1 && playerHarborArr[i - 2] > 0)) {

                var button = 0;
                switch (i) {
                    case 1:
                        button = new LeftExchangeButton(i, playerResCardsArr[this.leftButton4xResId - 1] >= 4);
                        exchangeLeftContDiv.appendChild(button.getDiv());
                        this.leftButtonArr.push(button.getLeftButtonDiv());
                        this.leftButton4xResIcon = button.getLeftButton4xResIcon();
                        break;
                    case 2:
                        button = new LeftExchangeButton(i, playerResCardsArr[this.leftButton3xResId - 1] >= 4);
                        exchangeLeftContDiv.appendChild(button.getDiv());
                        this.leftButtonArr.push(button.getLeftButtonDiv());
                        this.leftButton3xResIcon = button.getLeftButton3xResIcon();
                        break;
                    default:
                        button = new LeftExchangeButton(i, playerResCardsArr[i - 3] >= 2);
                        exchangeLeftContDiv.appendChild(button.getDiv());
                        this.leftButtonArr.push(button.getLeftButtonDiv());
                }

            } else {
                this.leftButtonArr.push(0);
            }
        }

        // --- botones de la derecha -------------------------------------------
        for (var i = 1; i < 6; i++) {
            var rightResource = document.createElement("img");
            rightResource.id = "right_res" + i;
            rightResource.className = "right_res";
            rightResource.src = "img/res" + i + "_150.png";

            rightResource.onclick = function(e) {
                var id = this.id.substr(this.id.length - 1);
                window.gui.selectRightResourcePopUp(id);
            }

            exchangeRightContDiv.appendChild(rightResource);
        }

        // ---------------------------------------------------------------------

        exchangeContDiv.appendChild(exchangeLeftContDiv);
        exchangeContDiv.appendChild(exchangeRightContDiv);

        var exchangeSubmit = generateDiv("exchange_submit", "button");
        exchangeSubmit.innerHTML = "&nbsp;Intercambiar&nbsp;";
        exchangeContDiv.appendChild(exchangeSubmit);

        this.popUpContDiv.appendChild(exchangeContDiv);

        this.exchangeLeftContDiv = exchangeLeftContDiv;
        this.exchangeRightContDiv = exchangeRightContDiv;

    }

    // -------------------------------------------------------------------------
    generateExchangeLeftTitleDiv() {
        var exchangeLeftTitleDiv = generateDiv("left_title", "exchange_title");
        exchangeLeftTitleDiv.innerText = "Dar";
        return exchangeLeftTitleDiv;
    }

    generateExchangeRightTitleDiv() {
        var exchangeRightTitleDiv = generateDiv("right_title", "exchange_title");
        exchangeRightTitleDiv.innerText = "Obtener";
        return exchangeRightTitleDiv;
    }

    // -------------------------------------------------------------------------

    selectLeftButton(id) {
        for (var i = 0; i < this.leftButtonArr.length; i++) {
            if (id == i) {
                this.leftButtonArr[i].className = "left_button selected";
            } else if (this.leftButtonSelected == i) {
                var terminacion = this.leftButtonArr[i].className.split(" ");
                if (terminacion[1] != "selected") {
                    this.leftButtonArr[i].className = "left_button " + terminacion[1];
                } else {
                    this.leftButtonArr[i].className = "left_button";
                }
            }
        }
        this.leftButtonSelected = id;
    }

    selectRightResource(id) {
        for (var i = 1; i < this.exchangeRightContDiv.children.length; i++) {
            if (id == i) {
                this.exchangeRightContDiv.children[i].className = "right_res selected";
            } else {
                this.exchangeRightContDiv.children[i].className = "right_res";
            }
        }
        this.rightResourceSelected = id;
    }

    next4xResource() {
        this.leftButton4xResId++;
        if (this.leftButton4xResId > 5) this.leftButton4xResId = 1;
        this.leftButton4xResIcon.src = "img/iRes" + this.leftButton4xResId + ".png";
    }

    prev4xResource() {
        this.leftButton4xResId--;
        if (this.leftButton4xResId < 1) this.leftButton4xResId = 5;
        this.leftButton4xResIcon.src = "img/iRes" + this.leftButton4xResId + ".png";
    }

    next3xResource() {
        this.leftButton3xResId++;
        if (this.leftButton3xResId > 5) this.leftButton3xResId = 1;
        this.leftButton3xResIcon.src = "img/iRes" + this.leftButton3xResId + ".png";
    }

    prev3xResource() {
        this.leftButton3xResId--;
        if (this.leftButton3xResId < 1) this.leftButton3xResId = 5;
        this.leftButton3xResIcon.src = "img/iRes" + this.leftButton3xResId + ".png";
    }
}
