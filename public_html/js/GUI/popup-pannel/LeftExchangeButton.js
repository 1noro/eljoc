
class LeftExchangeButton {

    type; // 1: 4x, 2: 3x, >2: 2x
    clickable;

    leftButtonContDiv;
    leftButtonDiv;
    leftButton4xResIcon;
    leftButton3xResIcon;

    clicked = false;
    leftButton4xResId = 1;
    leftButton3xResId = 1;

    constructor(_type, _clickable) {
        this.type = _type;
        this.clickable = _clickable;
        switch (this.type) {
            case 1:
                this.leftButtonContDiv = this.clickable ? this.generateleftButton4xOn(_type) : this.generateleftButton4xOff(_type);
                break;
            case 2:
                this.leftButtonContDiv = this.clickable ? this.generateleftButton3xOn(_type) : this.generateleftButton3xOff(_type);
                break;
            default:
                this.leftButtonContDiv = this.clickable ? this.generateleftButton2xOn(_type) : this.generateleftButton2xOff(_type);
        }
    }

    getDiv() {
        return this.leftButtonContDiv;
    }

    getLeftButtonDiv() {
        return this.leftButtonDiv;
    }

    getLeftButton4xResIcon() {
        return this.leftButton4xResIcon;
    }

    getLeftButton3xResIcon() {
        return this.leftButton3xResIcon;
    }

    // -------------------------------------------------------------------------

    generateleftButton4xOn(i) {
        var leftButtonContDiv = generateDiv("left_button_cont" + i, "left_button_cont");

        var leftButton = generateDiv("left_button" + (i - 1), "left_button");
        leftButton.innerText = getExchangeButtonText(i);
        leftButton.onclick = function(event) {window.gui.selectLeftButtonPopUp(this.id.substr(this.id.length - 1));};
        leftButtonContDiv.appendChild(leftButton);
        this.leftButtonDiv = leftButton;

        var leftButtonIconCont = generateDiv("left_button_icon_cont" + i, "left_button_icon_cont");

        var leftButtonIconArrowL = generateInlineArrow("left_button_arrowL" + i, true);
        leftButtonIconArrowL.onclick = function(event) {window.gui.prev4xResourcePopUp();};
        var leftButtonIconArrowR = generateInlineArrow("left_button_arrowR" + i, false);
        leftButtonIconArrowR.onclick = function(event) {window.gui.next4xResourcePopUp();};
        var leftButtonIcon = generateImg("left_button_icon" + i, "left_button_icon", "img/iRes" + this.leftButton4xResId + ".png");
        this.leftButton4xResIcon = leftButtonIcon;

        leftButtonIconCont.appendChild(leftButtonIconArrowL);
        leftButtonIconCont.appendChild(leftButtonIcon);
        leftButtonIconCont.appendChild(leftButtonIconArrowR);

        leftButtonContDiv.appendChild(leftButtonIconCont);
        return leftButtonContDiv;
    }

    generateleftButton4xOff(i) {
        var leftButtonContDiv = generateDiv("left_button_cont" + i, "left_button_cont");

        var leftButton = generateDiv("left_button" + (i - 1), "left_button off");
        leftButton.innerText = getExchangeButtonText(i);
        leftButtonContDiv.appendChild(leftButton);
        this.leftButtonDiv = leftButton;

        var leftButtonIconCont = generateDiv("left_button_icon_cont" + i, "left_button_icon_cont");

        var leftButtonIconArrowL = generateInlineArrow("left_button_arrowL" + i, true);
        leftButtonIconArrowL.onclick = function(event) {window.gui.prev4xResourcePopUp();};
        var leftButtonIconArrowR = generateInlineArrow("left_button_arrowR" + i, false);
        leftButtonIconArrowR.onclick = function(event) {window.gui.next4xResourcePopUp();};
        var leftButtonIcon = generateImg("left_button_icon" + i, "left_button_icon off", "img/iRes" + this.leftButton4xResId + "_off.png");
        this.leftButton4xResIcon = leftButtonIcon;

        leftButtonIconCont.appendChild(leftButtonIconArrowL);
        leftButtonIconCont.appendChild(leftButtonIcon);
        leftButtonIconCont.appendChild(leftButtonIconArrowR);

        leftButtonContDiv.appendChild(leftButtonIconCont);
        return leftButtonContDiv;
    }

    generateleftButton3xOn(i) {
        var leftButtonContDiv = generateDiv("left_button_cont" + i, "left_button_cont");

        var leftButton = generateDiv("left_button" + (i - 1), "left_button");
        leftButton.innerText = getExchangeButtonText(i);
        leftButton.onclick = function(event) {window.gui.selectLeftButtonPopUp(this.id.substr(this.id.length - 1));};
        leftButtonContDiv.appendChild(leftButton);
        this.leftButtonDiv = leftButton;

        var leftButtonIconCont = generateDiv("left_button_icon_cont" + i, "left_button_icon_cont");

        var leftButtonIconArrowL = generateInlineArrow("left_button_arrowL" + i, true);
        leftButtonIconArrowL.onclick = function(event) {window.gui.prev3xResourcePopUp();};
        var leftButtonIconArrowR = generateInlineArrow("left_button_arrowR" + i, false);
        leftButtonIconArrowR.onclick = function(event) {window.gui.next3xResourcePopUp();};
        var leftButtonIcon = generateImg("left_button_icon" + i, "left_button_icon", "img/iRes" + this.leftButton3xResId + ".png");
        this.leftButton3xResIcon = leftButtonIcon;

        leftButtonIconCont.appendChild(leftButtonIconArrowL);
        leftButtonIconCont.appendChild(leftButtonIcon);
        leftButtonIconCont.appendChild(leftButtonIconArrowR);

        leftButtonContDiv.appendChild(leftButtonIconCont);
        return leftButtonContDiv;
    }

    generateleftButton3xOff(i) {
        var leftButtonContDiv = generateDiv("left_button_cont" + i, "left_button_cont");

        var leftButton = generateDiv("left_button" + (i - 1), "left_button off");
        leftButton.innerText = getExchangeButtonText(i);
        leftButtonContDiv.appendChild(leftButton);
        this.leftButtonDiv = leftButton;

        var leftButtonIconCont = generateDiv("left_button_icon_cont" + i, "left_button_icon_cont");

        var leftButtonIconArrowL = generateInlineArrow("left_button_arrowL" + i, true);
        leftButtonIconArrowL.onclick = function(event) {window.gui.prev3xResourcePopUp();};
        var leftButtonIconArrowR = generateInlineArrow("left_button_arrowR" + i, false);
        leftButtonIconArrowR.onclick = function(event) {window.gui.next3xResourcePopUp();};
        var leftButtonIcon = generateImg("left_button_icon" + i, "left_button_icon off", "img/iRes" + this.leftButton3xResId + "_off.png");
        this.leftButton3xResIcon = leftButtonIcon;

        leftButtonIconCont.appendChild(leftButtonIconArrowL);
        leftButtonIconCont.appendChild(leftButtonIcon);
        leftButtonIconCont.appendChild(leftButtonIconArrowR);

        leftButtonContDiv.appendChild(leftButtonIconCont);
        return leftButtonContDiv;
    }

    generateleftButton2xOn(i) {
        var resId = (i - 2);
        var leftButtonContDiv = generateDiv("left_button_cont" + i, "left_button_cont");

        var leftButton = generateDiv("left_button" + (i - 1), "left_button");
        leftButton.innerText = getExchangeButtonText(i);
        leftButton.onclick = function(event) {window.gui.selectLeftButtonPopUp(this.id.substr(this.id.length - 1));};
        leftButtonContDiv.appendChild(leftButton);
        this.leftButtonDiv = leftButton;

        var leftButtonIconCont = generateDiv("left_button_icon_cont" + i, "left_button_icon_cont");

        var leftButtonIconArrowL = generateInlineSpaceSeparator();
        var leftButtonIconArrowR = generateInlineSpaceSeparator();
        var leftButtonIcon = generateImg("left_button_icon" + i, "left_button_icon", "img/iRes" + resId + ".png");

        leftButtonIconCont.appendChild(leftButtonIconArrowL);
        leftButtonIconCont.appendChild(leftButtonIcon);
        leftButtonIconCont.appendChild(leftButtonIconArrowR);

        leftButtonContDiv.appendChild(leftButtonIconCont);
        return leftButtonContDiv;
    }

    generateleftButton2xOff(i) {
        var resId = (i - 2);
        var leftButtonContDiv = generateDiv("left_button_cont" + i, "left_button_cont");
        var leftButton = generateDiv("left_button" + (i - 1), "left_button off");
        leftButton.innerText = getExchangeButtonText(i);
        leftButtonContDiv.appendChild(leftButton);
        this.leftButtonDiv = leftButton;

        var leftButtonIconCont = generateDiv("left_button_icon_cont" + i, "left_button_icon_cont");

        var leftButtonIconArrowL = generateInlineSpaceSeparator();
        var leftButtonIconArrowR = generateInlineSpaceSeparator();
        var leftButtonIcon = generateImg("left_button_icon" + i, "left_button_icon off", "img/iRes" + resId + "_off.png");

        leftButtonIconCont.appendChild(leftButtonIconArrowL);
        leftButtonIconCont.appendChild(leftButtonIcon);
        leftButtonIconCont.appendChild(leftButtonIconArrowR);

        leftButtonContDiv.appendChild(leftButtonIconCont);
        return leftButtonContDiv;
    }
}
