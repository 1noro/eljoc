
class Harbor {

    // variables dadas
    id; // identificador
    number; // identificador

    cw; // ancho de la celda de la cuadricula
    ch; // alto de la celda de la cuadricula
    rx; // posicion del centro del hexagono en la cuadricula (eje x) (relativa a la cuadricula)
    ry; // posicion del centro del hexagono en la cuadricula (eje y) (relativa a la cuadricula)

    displayType; // tipo de imagen del puerto (dependiendo de su posición en el hexágono)
    resourceType; // tipo de puerto (de que materia prima es)

    // variables calculadas
    vertexArr; // array de las coordenadas de los vertices (0 = x, 1 = y)
    width; // ancho de la imagen (el tamaño de una celda)
    height; // alto de la imagen (el tamaño de dos celdas)

    // variables preasignadas

    constructor(_id, _cw, _ch, _rx, _ry, _displayType, _resourceType) {
        this.id = "harbor" + _id;
        this.number = _id;
        this.cw = _cw;
        this.ch = _ch;
        this.rx = _rx;
        this.ry = _ry;
        this.displayType = _displayType;
        this.resourceType = _resourceType;

        this.vertexArr = new Array();
        this.vertexArr[0] = [this.rx * this.cw, this.ry * this.ch];

        this.width = _cw;
        this.height = _ch * 2;
    }

    getId() {
        return this.id;
    }

    draw(ctx) {
        var offsetX = 0;
        var offsetY = 0;
        if (this.displayType == 0) offsetX = this.ch * 0.3;
        if (this.displayType == 1) offsetX = (this.ch * 0.3) * (-1);
        if (this.displayType == 3) offsetX = (this.ch * 0.4) * (-1);
        if (this.displayType == 4) {offsetX = this.ch * 0.3; offsetY = (this.ch * 0.03) * (-1);}
        ctx.drawImage(
            document.getElementById('harbor' + this.displayType + '' + this.resourceType),
            this.vertexArr[0][0] - offsetX, // x
            this.vertexArr[0][1] - offsetY, // y
            this.width, // width
            this.height // height
        );
    }

}
