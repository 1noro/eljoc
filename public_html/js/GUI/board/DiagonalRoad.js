
class DiagonalRoad {

    // variables dadas
    id; // identificador
    number; // identificador

    cw; // ancho de la celda de la cuadricula
    ch; // alto de la celda de la cuadricula
    rx; // posicion de la esquina superior izquierda le la casilla del camino en la cuadricula (eje x) (relativa a la cuadricula)
    ry; // posicion de la esquina superior izquierda le la casilla del camino en la cuadricula (eje y) (relativa a la cuadricula)

    team; // de que equipo es el camino (eso implica el color)
    sideLeft; // si el camino es diagonal hacia la izquierda o no
    allowedColors; // colores que pueden construir este camino

    // variables calculadas
    width; // ancho del camino
    vertexDrawArr; // array de las coordenadas de los vertices (0 = x, 1 = y)
    vertexClickArr; // array de las coordenadas de los vertices (0 = x, 1 = y)

    // variables preasignadas
    active = false; // si está pulsado o no
    hidden = true; // si la pueblo está visible o no (puest o no)

    constructor(_id, _cw, _ch, _rx, _ry, _team, _sideLeft, _allowedColors) {
        this.id = "droad" + _id;
        this.number = _id;
        this.cw = _cw;
        this.ch = _ch;
        this.rx = _rx;
        this.ry = _ry;
        // this.team = (_team == 0) ? Math.floor(Math.random() * 4) + 1 : _team;
        this.team = _team;
        this.sideLeft = _sideLeft;
        this.allowedColors = _allowedColors;

        this.width = _ch / 4.5;

        this.vertexDrawArr = new Array();
        if (this.sideLeft) {
            this.vertexDrawArr[0] = [this.rx * this.cw, this.ry * this.ch];
            this.vertexDrawArr[1] = [this.rx * this.cw + this.cw, this.ry * this.ch + this.ch];
        } else {
            this.vertexDrawArr[0] = [this.rx * this.cw + this.cw, this.ry * this.ch];
            this.vertexDrawArr[1] = [this.rx * this.cw, this.ry * this.ch + this.ch];
        }

        this.vertexClickArr = new Array();
        this.vertexClickArr[0] = [this.rx * this.cw, this.ry * this.ch];
        this.vertexClickArr[1] = [this.rx * this.cw + this.cw, this.ry * this.ch + this.ch];
    }

    getId() {
        return this.id;
    }

    getActive() {
        return this.active;
    }

    setActive(_active) {
        this.active = _active;
    }

    getTeam() {
        return this.team;
    }

    setTeam(_team) {
        this.team = _team;
    }

    getAllowedColors() {
        return this.allowedColors;
    }

    setAllowedColors(_allowedColors) {
        this.allowedColors = _allowedColors;
    }

    addAllowedColor(_color) {
        if (!this.allowedColors.includes(_color)) this.allowedColors.push(_color);
    }

    checkIsColorAllowed(_color) {
        var out = false;
        for (var i = 0; i < this.allowedColors.length; i++) {
            if (this.allowedColors[i] == _color) {
                out = true;
                break;
            }
        }
        return out;
    }

    draw(ctx) {
        if (this.team != 0) {
            var color = "pink";
            if (this.team == 1) color = "#c3bc5c";
            else if (this.team == 2) color = "#6dc5ce";
            else if (this.team == 3) color = "#873dba";
            else if (this.team == 4) color = "#e95a47";

            ctx.beginPath();
            ctx.moveTo(this.vertexDrawArr[0][0], this.vertexDrawArr[0][1]);
            ctx.lineTo(this.vertexDrawArr[1][0], this.vertexDrawArr[1][1]);
            // ctx.closePath();

            ctx.lineWidth = this.width;
            ctx.strokeStyle = this.active ? "pink" : color;
            ctx.lineCap = "round";
            ctx.stroke();
        }
    }

    checkClick(cursorX_relM, cursorY_relM, color) {
        var out = false;

        // console.log("(checkClick) team: " + this.team + ", allowedColors: " + JSON.stringify(this.allowedColors) + ", checkIsColorAllowed(" + color + "): " + this.checkIsColorAllowed(color));

        if (
            this.team == 0 &&
            this.checkIsColorAllowed(color) &&
            cursorX_relM > this.vertexClickArr[0][0] && cursorY_relM > this.vertexClickArr[0][1] &&
            cursorX_relM < this.vertexClickArr[1][0] && cursorY_relM < this.vertexClickArr[1][1]
        ) out = true;

        return out;
    }

    activate() {
        this.active = true;
    }

    deactivate() {
        this.active = false;
    }

    show() {
        this.hidden = false;
    }

    hide() {
        this.hidden = true;
    }

    isHidden() {
        return this.hidden;
    }
}
