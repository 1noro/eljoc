
class Hex {

    // variables dadas
    id; // identificador
    number; // identificador

    cw; // ancho de la celda de la cuadricula
    ch; // alto de la celda de la cuadricula
    rx; // posicion del centro del hexagono en la cuadricula (eje x) (relativa a la cuadricula)
    ry; // posicion del centro del hexagono en la cuadricula (eje y) (relativa a la cuadricula)

    resourceType; // tipo de recurso que produce la casilla
    displayedNumber; // numero mostrado en el circulo

    // variables calculadas
    absCentX; // centro del hexagono absoluto en pixeles (eje x) (con respecto al canvas)
    absCentY; // centro del hexagono absoluto en pixeles (eje y) (con respecto al canvas)
    vertexArr; // array de las coordenadas de los vertices (0 = x, 1 = y)

    // variables preasignadas
    active = false; // si está pulsado o no
    thief = false; // si posee el ladrón o no

    constructor(_id, _cw, _ch, _rx, _ry, _resourceType, _displayedNumber) {
        this.id = "hex" + _id;
        this.number = _id;
        this.cw = _cw;
        this.ch = _ch;
        this.rx = _rx;
        this.ry = _ry;
        this.resourceType = _resourceType;
        this.displayedNumber = _displayedNumber;

        this.absCentX = this.cw * this.rx;
        this.absCentY = this.ch * this.ry;

        this.vertexArr = new Array();
        this.vertexArr[0] = [this.absCentX - this.cw, this.absCentY - this.ch];
        this.vertexArr[1] = [this.absCentX, this.absCentY - (this.ch * 2)];
        this.vertexArr[2] = [this.absCentX + this.cw, this.absCentY - this.ch];
        this.vertexArr[3] = [this.absCentX + this.cw, this.absCentY + this.ch];
        this.vertexArr[4] = [this.absCentX, this.absCentY + (this.ch * 2)];
        this.vertexArr[5] = [this.absCentX - this.cw, this.absCentY + this.ch];

        if (this.resourceType == 0) this.thief = true;

        // console.log(this.id + ": (" + this.vertexArr[0][0] + ", " + this.vertexArr[0][1] + ") (" + this.vertexArr[3][0] + ", " + this.vertexArr[3][1] + ")");
    }

    getId() {
        return this.id;
    }

    getActive() {
        return this.active;
    }

    setActive(_active) {
        this.active = _active;
    }

    getThief() {
        return this.thief;
    }

    setThief(_thief) {
        this.thief = _thief;
    }

    draw(ctx) {
        var imgId = this.active ? 'resourceA' : 'resource' + this.resourceType;
        ctx.drawImage(
            document.getElementById(imgId),
            this.absCentX - this.cw, // x
            this.absCentY - (this.ch * 2), // y
            this.cw * 2, // width
            this.ch * 4 // height
        );

        if (this.displayedNumber != 0) {
            ctx.fillStyle = (this.displayedNumber == 8 || this.displayedNumber == 6) ? "red" : "black";
            ctx.font = this.ch + "px PTSerif Regular";
            ctx.textAlign = "center";
            ctx.fillText(
                this.displayedNumber, // text
                this.absCentX, // x
                this.absCentY + (this.ch / 2.5) // y
            );
        }

        if (this.thief) {
            ctx.drawImage(
                document.getElementById('thief'),
                this.absCentX - this.ch * 0.75, // x
                this.absCentY - this.ch * 0.75, // y
                this.ch * 1.5, // width
                this.ch * 1.5 // height
            );
        }
    }

    checkClick(cursorX_relM, cursorY_relM) {
        var out = false;

        if (
            cursorX_relM > this.vertexArr[0][0] && cursorY_relM > this.vertexArr[0][1] &&
            cursorX_relM < this.vertexArr[3][0] && cursorY_relM < this.vertexArr[3][1]
        ) out = true;

        return out;
    }

    activate() {
        this.active = true;
    }

    deactivate() {
        this.active = false;
    }

    thiefOn() {
        this.thief = true;
    }

    thiefOff() {
        this.thief = false;
    }

}
