
class Town {

    // variables dadas
    id; // identificador
    number; // identificador

    cw; // ancho de la celda de la cuadricula
    ch; // alto de la celda de la cuadricula
    rx; // posicion del centro del hexagono en la cuadricula (eje x) (relativa a la cuadricula)
    ry; // posicion del centro del hexagono en la cuadricula (eje y) (relativa a la cuadricula)

    team; // de que equipo es el pueblo (eso implica el color)
    level; // (0, 1, 2) (oculto, pueblo, ciudad)
    blocked; // si la pueblo está bloqueado por uno cercano, por lo tanto no puede aparecer
    allowedColors; // colores que pueden construir este pueblo

    width; // ancho del cuadrado
    height; // alto del cuadrado

    // variables calculadas
    absCentX; // centro del cuadrdo absoluto en pixeles (eje x) (con respecto al canvas)
    absCentY; // centro del cuadrdo absoluto en pixeles (eje y) (con respecto al canvas)
    vertexArr; // array de las coordenadas de los vertices (0 = x, 1 = y)

    // variables preasignadas
    active = false; // si está pulsado o no
    resourcesList = [];
    nextHarbor = 6; // 6 = ningún puerto (ver la lista para mas info)

    constructor(_id, _cw, _ch, _rx, _ry, _team, _level, _blocked, _allowedColors) {
        this.id = "town" + _id;
        this.number = _id;
        this.cw = _cw;
        this.ch = _ch;
        this.rx = _rx;
        this.ry = _ry;
        // this.team = (_team == 0) ? Math.floor(Math.random() * 4) + 1 : _team;
        this.team = _team;
        this.level = _level;
        this.blocked = _blocked == 0 ? false : true;
        this.allowedColors = _allowedColors;

        this.width = _ch * 1.2;
        this.height = _ch * 1.2;

        this.absCentX = this.cw * this.rx;
        this.absCentY = this.ch * this.ry;

        this.vertexArr = new Array();
        this.vertexArr[0] = [this.absCentX - (this.width / 2), this.absCentY - (this.height / 2)];
        this.vertexArr[1] = [this.absCentX + (this.width / 2), this.absCentY + (this.height / 2)];
    }

    getId() {
        return this.id;
    }

    getActive() {
        return this.active;
    }

    setActive(_active) {
        this.active = _active;
    }

    getLevel() {
        return this.level;
    }

    setLevel(_level) {
        this.level = _level;
    }

    getTeam() {
        return this.team;
    }

    setTeam(_team) {
        this.allowedColors = [_team];
        this.team = _team;
    }

    getBlocked() {
        return this.blocked;
    }

    isBlocked() {
        return this.blocked;
    }

    setBlocked(_blocked) {
        if (Number.isInteger(parseInt(_blocked)))
            this.blocked = parseInt(_blocked) == 0 ? false : true;
        else this.blocked = _blocked;
    }

    getResourcesList() {
        return this.resourcesList;
    }

    setResourcesList(_resourcesList) {
        this.resourcesList = _resourcesList;
    }

    addResource2List(_resuourceNum) {
        this.resourcesList.push(_resuourceNum);
    }

    setAllowedColors(_allowedColors) {
        this.allowedColors = _allowedColors;
    }

    addAllowedColor(_color) {
        // console.log("[####] this.allowedColors: " + JSON.stringify(this.allowedColors));
        // console.log("[####] !this.allowedColors.includes(" + _color + ")? " + (!this.allowedColors.includes(_color)));
        if (!this.allowedColors.includes(_color)) this.allowedColors.push(_color);
        // console.log("[####] this.allowedColors: " + JSON.stringify(this.allowedColors));
    }

    checkIsColorAllowed(_color) {
        var out = false;
        for (var i = 0; i < this.allowedColors.length; i++) {
            if (this.allowedColors[i] == _color) {
                out = true;
                break;
            }
        }
        return out;
    }

    getDataForTownsSQLArr() {
        return [this.team, this.level, this.blocked ? 1 : 0, this.allowedColors];
    }

    draw(ctx) {
        if (this.level != 0) {
            var imgId = 'town' + this.team;
            if (this.level == 1) imgId = 'town' + this.team;
            if (this.level == 2) imgId = 'city' + this.team;
            ctx.drawImage(
                document.getElementById(imgId),
                this.vertexArr[0][0], // x
                this.vertexArr[0][1], // y
                this.width, // width
                this.height // height
            );
        }
    }

    checkClick(cursorX_relM, cursorY_relM, color) {
        var out = false;

        // console.error("blocked: " + this.blocked);

        if (
            !this.blocked &&
            cursorX_relM > this.vertexArr[0][0] && cursorY_relM > this.vertexArr[0][1] &&
            cursorX_relM < this.vertexArr[1][0] && cursorY_relM < this.vertexArr[1][1]
        ) {
            if (
                this.checkIsColorAllowed(color) ||
                window.g.getData().game.phase == 3 ||
                window.g.getData().game.phase == 4
            ) out = true;
        }

        return out;
    }

    activate() {
        this.active = true;
    }

    deactivate() {
        this.active = false;
    }

    show() {
        this.level = 1;
    }

    upgrade() {
        this.level++;
    }

}
