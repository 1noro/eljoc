
class LoginForm {

    active = false;

    loginFormContDiv;
    playerIdInput;
    gameIdInput;

    topMsgTitleDiv;

    constructor() {
        this.loginFormContDiv = this.generateLoginFormContDiv();
    }

    generateLoginFormContDiv() {

        // <div id="login_form_cont" class="item menu">
        //     <div class="item">
        //         <img src="img/logoIni_300.png" alt="logo">
        //     </div>
        //     <div class="item">
        //         <input id="player_id_input" type="number" min="1" max="255" name="" value="" placeholder="Id de jugador" title="Valor numérico (Sugerencia: 1)">
        //     </div>
        //     <div class="item">
        //         <input id="game_id_input" type="number" min="1" max="255" name="" value="" placeholder="Id de partida" title="Valor numérico (Sugerencia: 1)">
        //     </div>
        //     <div class="item">
        //         <div id="login_submit_button" class="button">
        //             Entrar
        //         </div>
        //     </div>
        //     <div class="item">
        //         <div id="login_random_button" class="button">
        //             Aleatorio
        //         </div>
        //     </div>
        // </div>

        var loginFormContDiv = document.createElement("div");
        loginFormContDiv.id = "login_form_cont";
        loginFormContDiv.className = "item menu";

        var logoContDiv = document.createElement("div");
        logoContDiv.className = "item";

        var logoImg = document.createElement("img");
        logoImg.alt = "logo";
        logoImg.src = "img/logoIni_300.png";

        logoImg.onload = function(e) {
            // ocultamos la pantalla de carga
            window.joc.loadingScreen.style.display = "none";
        }

        logoContDiv.appendChild(logoImg);

        var playerIdInputContDiv = document.createElement("div");
        playerIdInputContDiv.className = "item";

        var playerIdInput = document.createElement("input");
        playerIdInput.id = "player_id_input";
        playerIdInput.type = "number";
        playerIdInput.min = "1";
        playerIdInput.max = "18446744073709551615"; // MariaDB: SELECT ~0;
        playerIdInput.name = "";
        playerIdInput.value = window.joc.testing ? window.joc.randomPlayerId : "";
        playerIdInput.placeholder = "Id de jugador";
        playerIdInput.title = "Valor numérico (Sugerencia: " + window.joc.randomPlayerId + ")";

        playerIdInput.onkeydown = function(e) {
            if (isEnterKeyPressed(e)) {
                var playerId = window.gui.getLoginFormPlayerId();
                var gameId = window.gui.getLoginFormGameId();
                window.g.loginSubmit(playerId, gameId);
            }
        };

        playerIdInputContDiv.appendChild(playerIdInput);

        var gameIdInputContDiv = document.createElement("div");
        gameIdInputContDiv.className = "item";

        var gameIdInput = document.createElement("input");
        gameIdInput.id = "game_id_input";
        gameIdInput.type = "number";
        gameIdInput.min = "1";
        gameIdInput.max = "18446744073709551615"; // MariaDB: SELECT ~0;
        gameIdInput.name = "";
        gameIdInput.value = window.joc.testing ? "790859786" : "";
        gameIdInput.placeholder = "Id de partida";
        gameIdInput.title = "Valor numérico (Sugerencia: " + window.joc.randomGameId + ")";

        gameIdInput.onkeydown = function(e) {
            if (isEnterKeyPressed(e)) {
                var playerId = window.gui.getLoginFormPlayerId();
                var gameId = window.gui.getLoginFormGameId();
                window.g.loginSubmit(playerId, gameId);
            }
        };

        gameIdInputContDiv.appendChild(gameIdInput);

        var loginSubmitButtonContDiv = document.createElement("div");
        loginSubmitButtonContDiv.className = "item";

        var loginSubmitButton = document.createElement("div");
        loginSubmitButton.id = "login_submit_button";
        loginSubmitButton.className = "button";
        loginSubmitButton.innerText = "Entrar";

        loginSubmitButton.onclick = function() {
            var playerId = window.gui.getLoginFormPlayerId();
            var gameId = window.gui.getLoginFormGameId();
            window.g.loginSubmit(playerId, gameId);
        };

        loginSubmitButtonContDiv.appendChild(loginSubmitButton);

        var loginRandomButtonContDiv = document.createElement("div");
        loginRandomButtonContDiv.className = "item";

        var loginRandomButton = document.createElement("div");
        loginRandomButton.id = "login_random_button";
        loginRandomButton.className = "button";
        loginRandomButton.innerText = "Aleatorio";

        loginRandomButton.onclick = function() {
            var playerId = window.joc.randomPlayerId;
            var gameId = window.joc.randomGameId;
            window.g.loginSubmit(playerId, gameId);
        };

        loginRandomButtonContDiv.appendChild(loginRandomButton);

        loginFormContDiv.appendChild(logoContDiv);
        loginFormContDiv.appendChild(playerIdInputContDiv);
        loginFormContDiv.appendChild(gameIdInputContDiv);
        loginFormContDiv.appendChild(loginSubmitButtonContDiv);
        loginFormContDiv.appendChild(loginRandomButtonContDiv);

        this.playerIdInput = playerIdInput;
        this.gameIdInput = gameIdInput;

        return loginFormContDiv;
    }

    getContDiv() {
        return this.loginFormContDiv;
    }

    activate() {
        this.active = true;
    }

    deactivate() {
        this.active = false;
    }

    show(_msg) {
        this.activate();
        if (_msg) window.gui.showTopMsg(_msg);
            else window.gui.cleanTopMsg();
        this.loginFormContDiv.style.display = "block";
    }

    hide() {
        this.deactivate();
        this.loginFormContDiv.style.display = "none";
    }

    getPlayerId() {
        return this.playerIdInput.value;
    }

    getGameId() {
        return this.gameIdInput.value;
    }

}
