
class Board {

    boardCanvasContDiv;
    canvas;

    canvasMarginTop;

    dbw; // displayed board Width
    dbh; // displayed board Height
    rbw; // real board Width
    rbh; // real board Height

    cw; // ancho de la celda de la cuadricula
    ch; // alto de la celda de la cuadricula

    clickable = false;
    clickCount = 0; // numero de clicks permitidos

    build = false;

    // ARRAYS de elementos
    harborArr;
    hexArr;

    droadArr;
    vroadArr;
    townArr;

    constructor(_canvasMarginTop, _canvas, _dbw, _dbh, _rbw, _rbh, _cw, _ch) {
        this.boardCanvasContDiv = document.getElementById('board_canvas_cont');

        this.canvasMarginTop = _canvasMarginTop;
        this.canvas = _canvas;

        this.dbw = _dbw;
        this.dbh = _dbh;
        this.rbw = _rbw;
        this.rbh = _rbh;

        this.cw = _cw;
        this.ch = _ch;
    }

    getBoardW() {
        return this.boardW;
    }

    getBoardH() {
        return this.boardH;
    }

    getBoardTop() {
        return this.boardTop;
    }

    getBoardLeft() {
        return (window.innerWidth - _boardW) / 2;
    }

    getCanvas() {
        return this.canvas;
    }

    getClickable() {
        return this.clickable;
    }

    setClickable(_clickable) {
        this.clickable = _clickable;
    }

    getBuild() {
        return this.build;
    }

    setBuild(_build) {
        this.build = _build;
    }

    getDroadArr() {
        return this.droadArr;
    }

    getVroadArr() {
        return this.vroadArr;
    }

    getTownArr() {
        return this.townArr;
    }

    isClickable() {
        return this.clickable;
    }

    clickableOn() {
        this.clickable = true;
    }

    clickableOff() {
        this.clickable = false;
    }

    getClickCount() {
        return this.clickCount;
    }

    setClickCount(_clickCount) {
        this.clickCount = _clickCount;
        if (this.clickCount <= 0) this.clickableOff();
            else this.clickableOn();
    }

    resetClickCount() {
        this.clickCount = 0;
        if (this.clickCount <= 0) this.clickableOff();
    }

    oneClick() {
        this.clickCount--;
        if (this.clickCount <= 0) this.clickableOff();
    }

    buildBoardObjects() {
        this.build = true;
        // creamos los puertos
        this.harborArr = new Array();
        for (var i = 0; i < window.joc.coordHarbor.length; i++) {
            this.harborArr[i] = new Harbor(
                i, this.cw, this.ch, window.joc.coordHarbor[i][0], window.joc.coordHarbor[i][1], window.joc.coordHarbor[i][2], window.joc.coordHarbor[i][3]
            );
        }
        // creamos los hexagonos
        this.hexArr = new Array();
        for (var i = 0; i < window.joc.coordHex.length; i++) {
            this.hexArr[i] = new Hex(
                i, this.cw, this.ch, window.joc.coordHex[i][0], window.joc.coordHex[i][1], window.joc.defaultHexResNum[i][0], window.joc.defaultHexResNum[i][1]
            );
        }
        // creamos los caminos diagonales
        this.droadArr = new Array();
        for (var i = 0; i < window.joc.coordDRoad.length; i++) {
            var color = window.g.getData().game.droads[i][0];
            var allowedColors = window.g.getData().game.droads[i][1];
            this.droadArr[i] = new DiagonalRoad(
                i, this.cw, this.ch, window.joc.coordDRoad[i][0], window.joc.coordDRoad[i][1], color, window.joc.coordDRoad[i][2], allowedColors
            );
        }
        // creamos los caminos verticales
        this.vroadArr = new Array();
        for (var i = 0; i < window.joc.coordVRoad.length; i++) {
            var color = window.g.getData().game.vroads[i][0];
            var allowedColors = window.g.getData().game.vroads[i][1];
            this.vroadArr[i] = new VerticalRoad(
                i, this.cw, this.ch, window.joc.coordVRoad[i][0], window.joc.coordVRoad[i][1], color, [[0, 0], [0, 0]], allowedColors
            );
        }
        // creamos los pueblos
        this.townArr = new Array();
        for (var i = 0; i < window.joc.coordTown.length; i++) {
            var fc = num2posTown(i);
            var fcNode = window.g.getData().game.towns[fc.f][fc.c];
            this.townArr[i] = new Town(
                i, this.cw, this.ch, window.joc.coordTown[i][0], window.joc.coordTown[i][1], fcNode[0], fcNode[1], fcNode[2], fcNode[3]
            );
        }

        // obtenemos los recursos de cada hexágono y se los asignamos a cada pueblo
        for (var i = 0; i < this.townArr.length; i++) {
            var fc = num2posTown(i);
            var hexList = posTown2hexNumList(fc.f, fc.c);
            for (var e = 0; e < hexList.length; e++) {
                this.townArr[i].addResource2List(window.joc.defaultHexResNum[hexList[e]][0]);
            }
        }
        console.log("[INFO] Se ha calculado el tablero");
        // console.log("los recursos de la ciudad 10 son: " + JSON.stringify(this.townArr[posTown2num(1, 0)].getResourcesList()));
        // console.log("los recursos de la ciudad 52 son: " + JSON.stringify(this.townArr[posTown2num(5, 2)].getResourcesList()));
    }

    reloadBoardObjects() {
        // actualizamos los caminos diagonales según lo que nos ha llegado de la bse de datos
        for (var i = 0; i < this.droadArr.length; i++) {
            var color = window.g.getData().game.droads[i][0];
            var allowedColors = window.g.getData().game.droads[i][1];
            if (color != 0) { // si no son de ningún color no hay que recalcularlos
                this.droadArr[i].setTeam(color);
                this.droadArr[i].setAllowedColors(allowedColors);
            }
        }
        // actualizamos los caminos diagonales según lo que nos ha llegado de la bse de datos
        for (var i = 0; i < this.vroadArr.length; i++) {
            var color = window.g.getData().game.vroads[i][0];
            var allowedColors = window.g.getData().game.droads[i][1];
            if (color != 0) { // si no son de ningún color no hay que recalcularlos
                this.vroadArr[i].setTeam(color);
                this.vroadArr[i].setAllowedColors(allowedColors);
            }
        }
        // actualizamos los pueblos según lo que nos ha llegado de la bse de datos
        for (var i = 0; i < this.townArr.length; i++) {
            var fc = num2posTown(i);
            var fcNode = window.g.getData().game.towns[fc.f][fc.c];
            if (fcNode[0] != 0 || fcNode[2] == 1) { // si no son de ningún color o no estan bloqueadas no hay que recalcularlas
                this.townArr[i].setTeam(fcNode[0]);
                this.townArr[i].setLevel(fcNode[1]);
                this.townArr[i].setBlocked(fcNode[2]);
                this.townArr[i].setAllowedColors(fcNode[3]);
            }
        }
        console.log("[INFO] Se ha recalculado el tablero");
    }

    draw(ctx) {
        if (!this.build) {
            this.buildBoardObjects();
            this.boardCanvasContDiv.style.display = "block";
        } else this.reloadBoardObjects();

        // dibujamos el fondo
        resetCanvasWithBgColor(window.joc.ctx, window.joc.rbw, window.joc.rbh, window.joc.bgColor );
        // dibujamos los puertos
        for (var i = 0; i < this.harborArr.length; i++) this.harborArr[i].draw(ctx);
        // dibujamos los hexagonos
        for (var i = 0; i < this.hexArr.length; i++) this.hexArr[i].draw(ctx);
        // dibujamos los caminos diagonales
        for (var i = 0; i < this.droadArr.length; i++) this.droadArr[i].draw(ctx);
        // dibujamos los caminos verticales
        for (var i = 0; i < this.vroadArr.length; i++) this.vroadArr[i].draw(ctx);
        // dibujamos los pueblos
        for (var i = 0; i < this.townArr.length; i++) this.townArr[i].draw(ctx);

        // mostramos el canvas si no esta visible aún
        if (this.canvas.style.display != "block") this.canvas.style.display = "block";
    }

}
