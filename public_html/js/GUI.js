// Graphical User Interface

class GUI {

    board;

    bottomPannel;
    leftPannel;
    middlePannel;
    rightPannel;
    topPannel;
    popUpPannel;

    constructor() {
        this.bottomPannel = new BottomPannel();
        this.leftPannel = new LeftPannel();
        this.middlePannel = new MiddlePannel();
        this.rightPannel = new RightPannel();
        this.topPannel = new TopPannel();
        this.popUpPannel = new PopUpPannel();

        this.board = new Board(
            window.joc.canvasMarginTop,
            window.joc.boardCanvas,
            window.joc.dbw,
            window.joc.dbh,
            window.joc.rbw,
            window.joc.rbh,
            window.joc.cw,
            window.joc.ch
        );

    }

    getBoard() {
        return this.board;
    }

    getBottomPannel() {
        return this.bottomPannel;
    }

    getLeftPannel() {
        return this.leftPannel;
    }

    getMiddlePannel() {
        return this.middlePannel;
    }

    getRightPannel() {
        return this.rightPannel;
    }

    getTopPannel() {
        return this.topPannel;
    }

    getPopUpPannel() {
        return this.popUpPannel;
    }

    // -------------------------------------------------------------------------

    generateBoard() {
        this.board = 0;
    }

    showLoginForm(_msg) {
        this.middlePannel.showLoginForm(_msg);
    }

    showWaitingGif() {
        this.middlePannel.showWaitingGif();
    }

    hideMiddlePannel() {
        this.middlePannel.hide();
    }

    hideMiddlePannel() {
        this.middlePannel.hide();
    }

    displayWaitBar() {
        this.bottomPannel.displayWaitBar();
    }

    showTopMsg(_msg) {
        this.topPannel.showTopMsg(_msg);
    }

    cleanTopMsg() {
        this.topPannel.cleanTopMsg();
    }

    displayReadyButton() {
        this.bottomPannel.displayReadyButton();
    }

    refreshOpponentItems(opponents, waiting) {
        this.rightPannel.refreshOpponentItems(opponents, waiting);
    }

    displayGameInfo(player_id, player_color, game_id) {
        this.rightPannel.displayGameInfo(player_id, player_color, game_id);
    }

    drawBoard() {
        this.board.draw(window.joc.ctx);
    }

    displayDices() {
        this.bottomPannel.displayDices();
    }

    isBoardClickable() {
        return this.board.isClickable();
    }

    setBoardClickCount(num) {
        this.board.setClickCount(num);
    }

    hideBottomPannel() {resources_pannel
        this.bottomPannel.hide();
    }

    getLoginFormPlayerId() {
        return this.middlePannel.getLoginForm().getPlayerId();
    }

    getLoginFormGameId() {
        return this.middlePannel.getLoginForm().getGameId();
    }

    getTownArr() {
        return this.board.getTownArr();
    }

    getDroadArr() {
        return this.board.getDroadArr();
    }

    getVroadArr() {
        return this.board.getVroadArr();
    }

    send1Click2Board() {
        this.board.oneClick();
    }

    switchLeftPannelState() {
        this.leftPannel.switchState();
    }

    addResourceCard(resourceId, number2add) {
        this.leftPannel.addResourceCard(resourceId, number2add);
    }

    removeResourceCard(resourceId, number2remove) {
        this.leftPannel.removeResourceCard(resourceId, number2remove);
    }

    addDevelopmentCard(developmentId, number2add) {
        this.leftPannel.addDevelopmentCard(developmentId, number2add);
    }

    removeDevelopmentCard(developmentId, number2remove) {
        this.leftPannel.removeDevelopmentCard(developmentId, number2remove);
    }

    showLeftPannel() {
        this.leftPannel.show();
    }

    hideLeftPannel() {
        this.leftPannel.hide();
    }

    closePopUp() {
        this.popUpPannel.close();
    }

    simpleClosePopUp() {
        this.popUpPannel.simpleClose();
    }

    selectLeftButtonPopUp(id) {
        this.popUpPannel.selectLeftButton(id);
    }

    selectRightResourcePopUp(id) {
        this.popUpPannel.selectRightResource(id);
    }

    next4xResourcePopUp() {
        this.popUpPannel.next4xResource();
    }

    prev4xResourcePopUp() {
        this.popUpPannel.prev4xResource();
    }

    next3xResourcePopUp() {
        this.popUpPannel.next3xResource();
    }

    prev3xResourcePopUp() {
        this.popUpPannel.prev3xResource();
    }
}
