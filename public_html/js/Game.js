
class Game {

    d; // datos de la partida en un diccionario formato JSON
    title; // Título de la página

    valueDice1 = 0;
    valueDice2 = 0;

    clickBehavior;

    constructor(_default_data, _title) {
        this.d = _default_data;
        this.title = _title;

        this.clickBehavior = new ClickBehavior();

        this.d.player.id = checkAndGetParamGET("player");
        this.d.game.id = checkAndGetParamGET("game");

        if (this.d.player.id != 0 && this.d.game.id != 0) {
            readFromPHP("join_game", this.getDataJSON());
        } else {
            console.log("[INFO] player.id y game.id no están definidos, mostrando el formulario de login");
            window.gui.showLoginForm();
        }
    }

    getData() {
        return this.d;
    }

    setData(_new_data) {
        this.d = _new_data;
    }

    getDataJSON() {
        return JSON.stringify(this.d);
    }

    setDataFromJSON(_new_data) {
        this.d = JSON.parse(_new_data);
    }

    setDicesValue(_valueDice1, _valueDice2) {
        this.valueDice1 = _valueDice1;
        this.valueDice2 = _valueDice2;
    }

    getValueDice1() {
        return this.valueDice1;
    }

    getValueDice2() {
        return this.valueDice2;
    }

    getValueDicesSum() {
        return this.valueDice1 + this.valueDice2;
    }

    getClickBehavior() {
        return this.clickBehavior;
    }

    // -------------------------------------------------------------------------

    analyzeData(_exec, _new_data) {

        if (this.d.game.phase > 0 && _new_data.game.phase == 0)
            location.reload(window.location.href);

        if (_exec == "overwrite_d") this.d = _new_data;
        else if (_exec == "join_ok") {
            this.d = _new_data;
            this.phaseDetector();
        } else if (_exec == "join_fail") {
            this.d = _new_data;
            window.gui.showLoginForm("No te puedes unir a esa partida");
        } else if (_exec == "set_ready") {
            window.gui.displayWaitBar();
        } else if (_exec == "ping_and_check") {
            // console.log(JSON.stringify(_new_data.game.towns[0][1]));
            this.d = _new_data;
            this.phaseDetector();
        }
    }

    // -------------------------------------------------------------------------

    phaseDetector() {
        var p = parseInt(this.d.game.phase);
        if (p == 0) {
            console.log("[PHA0] waitingForUsers");
            this.waitingSequence();
        } else if (p == 1) {
            console.log("[PHA1] startGame");
            if (parseInt(this.d.game.waiting) == 0) this.startGame();
        } else if (p == 2) {
            console.log("[PHA2] turnAssignmentRound");
            this.turnAssignmentRound();
        } else if (p == 3) {
            console.log("[PHA3] placeAllocationRound0");
            this.placeAllocationRound0();
        } else if (p == 4) {
            console.log("[PHA4] placeAllocationRound1");
            this.placeAllocationRound1();
        } else if (p == 5) {
            console.log("[PHA5] game");
        } else console.error("[FAIL] phase not in list: " + parseInt(this.d.phase));
    }

    // -------------------------------------------------------------------------

    waitingSequence() {
        window.gui.showWaitingGif();
        window.gui.showTopMsg("Esperando a los demás jugadores");
        if (this.d.player.ready) window.gui.displayWaitBar();
            else window.gui.displayReadyButton();
        window.gui.refreshOpponentItems(this.d.opponents, this.d.game.waiting);
        window.gui.displayGameInfo(this.d.player.id, this.d.player.color, this.d.game.id);
    }

    startGame() {
        console.log("[INFO] La partida se está iniciando");
        // empieza a tirar los dados el que tiene el color 1 (el primero que entró en la partida)
        if (parseInt(this.d.player.color) == 1) {
            this.d.player.turn = 1;
            // creamos el orden para tirar los dados
            var provisional_order = new Array();
            provisional_order.push(this.d.player.id);
            for (var i = 0; i < this.d.opponents.length; i++) {
                provisional_order.push(this.d.opponents[i].id);
            }
            this.d.game.order = provisional_order;
            readFromPHP("start_game", this.getDataJSON());
        }
        window.gui.showTopMsg("Esperando a los demás jugadores");
        window.gui.displayWaitBar();
    }

    turnAssignmentRound() {
        window.gui.hideMiddlePannel();
        window.gui.showLeftPannel();
        window.gui.drawBoard(); // dibujamos el tablero

        if (parseInt(this.d.player.turn) == 1) {
            window.gui.showTopMsg("Tu turno, tira los dados");
            window.gui.displayDices();
            this.clickBehavior.setDicesAllowClick(true);
        } else {
            window.gui.showTopMsg("Turno del Jugador " + getColorStr(this.getOpponentColorTurn()));
            window.gui.displayWaitBar();
        }
        window.gui.refreshOpponentItems(this.d.opponents, this.d.game.waiting);
    }

    placeAllocationRound0() {
        window.gui.showLeftPannel();
        window.gui.drawBoard(); // dibujamos el tablero
        if (parseInt(this.d.player.turn) == 1) {
            window.gui.showTopMsg("Tu turno, coloca un pueblo");
            if (!window.gui.isBoardClickable()) {
                window.gui.setBoardClickCount(1);
            }
            window.gui.hideBottomPannel();
        } else {
            window.gui.showTopMsg("Turno del Jugador " + getColorStr(this.getOpponentColorTurn()));
            window.gui.displayWaitBar();
        }
        window.gui.refreshOpponentItems(this.d.opponents, this.d.game.waiting);
    }

    placeAllocationRound1() {
        window.gui.showLeftPannel();
        window.gui.drawBoard(); // dibujamos el tablero
        if (parseInt(this.d.player.turn) == 1) {
            window.gui.showTopMsg("Tu turno, coloca otro pueblo y dos caminos");
            if (!window.gui.isBoardClickable()) {
                window.gui.setBoardClickCount(3);
            }
            window.gui.hideBottomPannel();

            if (this.clickBehavior.getTownBuild() && this.clickBehavior.getRoadsBulid() >= 2) {
                // detectamos si se acabó la ronda
                if (this.d.player.id == this.d.game.order[this.d.game.order.length - 1]) {
                    this.passTurn();
                    this.nextPhase();
                } else this.passTurn();
            }
        } else {
            window.gui.showTopMsg("Turno del Jugador " + getColorStr(this.getOpponentColorTurn()));
            window.gui.displayWaitBar();
        }
        window.gui.refreshOpponentItems(this.d.opponents, this.d.game.waiting);
    }

    // -------------------------------------------------------------------------

    registerTmpData(_tag, _data) {
        this.getData().tmp.push({"tag" : _tag, "data" : _data});
        readFromPHP("register_tmp_data", this.getDataJSON());
        this.getData().tmp = new Array();
    }

    passTurn() {
        readFromPHP("pass_turn", this.getDataJSON());
    }

    nextPhase() {
        readFromPHP("next_phase", this.getDataJSON());
    }

    setTowns() {
        readFromPHP("set_towns", this.getDataJSON());
    }

    setDRoads() {
        readFromPHP("set_droads", this.getDataJSON());
    }

    setVRoads() {
        readFromPHP("set_vroads", this.getDataJSON());
    }

    endPhase2(_tag, _data) {
        this.getData().tmp.push({"tag" : _tag, "data" : _data});
        readFromPHP("end_phase_2", this.getDataJSON());
        this.getData().tmp = new Array();
        console.log("[INFO] End of phase 2 (turnAssignmentRound)");
    }

    loginSubmit(_playerId, _gameId) {
        if (isDefinedNumber(_playerId) && isDefinedNumber(_gameId)) {
            this.d.player.id = parseInt(_playerId);
            console.log("[ SET] player = " + _playerId);
            this.d.game.id = parseInt(_gameId);
            console.log("[ SET] game = " + _gameId);
            window.gui.getMiddlePannel().hideLoginForm();
            readFromPHP("join_game", this.getDataJSON());
        } else {
            console.error("[FAIL] playerId: '" + _playerId + "' o gameId: '" + _gameId + "', o los dos no son numéricos");
        }
    }

    // -------------------------------------------------------------------------

    getOpponentIdTurn() {
        var out = 0;
        for (var i = 0; i < this.d.opponents.length; i++) {
            if (parseInt(this.d.opponents[i].turn) == 1) {
                out = this.d.opponents[i].id;
                break;
            }
        }
        return out;
    }

    getOpponentColorTurn() {
        var out = 0;
        for (var i = 0; i < this.d.opponents.length; i++) {
            if (parseInt(this.d.opponents[i].turn) == 1) {
                out = this.d.opponents[i].color;
                break;
            }
        }
        return out;
    }

    detectEndOfRound() {
        var out = false;
        if (this.d.player.id == this.d.game.order[this.d.game.order.length - 1])
            out = true;
        return out;
    }

}
