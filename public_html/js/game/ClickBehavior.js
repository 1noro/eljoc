
class ClickBehavior {

    townBulid = false;
    roadsBulid = 0;

    dicesAllowClick = false;

    constructor() {}

    getTownBuild() {
        return this.townBulid;
    }

    getRoadsBulid() {
        return this.roadsBulid;
    }

    setDicesAllowClick(_dicesAllowClick) {
        this.dicesAllowClick = _dicesAllowClick;
    }

    getDicesAllowClick() {
        return this.dicesAllowClick;
    }

    // cx - cursorX_relM
    // cy - cursorY_relM
    parser(cx, cy, phase, round) {
        var click_detectado = false;

        if (phase == 3) {
            click_detectado = this.placeAllocationRound0(cx, cy);
        } else if (phase == 4) {
            click_detectado = this.placeAllocationRound1(cx, cy);
        } else {
            console.error("[FAIL] (ClickBehavior) phase (" + phase + ") and round (" + round + ") not implemented.");
        }

        if (click_detectado) window.gui.drawBoard();
    }

    // -------------------------------------------------------------------------

    detectAndBlockAdjacentTowns(ifc) {
        var blockedTowns = getBlockedTowns(ifc.f, ifc.c);
        for (var i = 0; i < blockedTowns.length; i++) {
            var blockedTownNum = posTown2num(blockedTowns[i].f, blockedTowns[i].c);
            window.gui.getTownArr()[blockedTownNum].setBlocked(true);
            var allowedColors = window.g.getData().game.towns[blockedTowns[i].f][blockedTowns[i].c][3];
            window.g.getData().game.towns[blockedTowns[i].f][blockedTowns[i].c] = [0, 0, 1, allowedColors];
        }
    }

    addAllowedColor2RoadsFromTown(ifc, color) {
        var allowedDRoads = townFC2nDRoad(ifc.f, ifc.c);
        var allowedVRoad = townFC2nVRoad(ifc.f, ifc.c);

        for (var i = 0; i < allowedDRoads.length; i++) {
            window.gui.getDroadArr()[allowedDRoads[i]].addAllowedColor(color);
            window.g.getData().game.droads[allowedDRoads[i]][1].push(color);
        }

        window.gui.getVroadArr()[allowedVRoad].addAllowedColor(color);
        window.g.getData().game.vroads[allowedVRoad][1].push(color);
    }

    addAllowedColor2RoadsFromRoad(arr, roadI, color) {
        var town0FC = {"f" : arr[roadI][0][0], "c" : arr[roadI][0][1]};
        var town1FC = {"f" : arr[roadI][1][0], "c" : arr[roadI][1][1]};
        var town0 = window.gui.getTownArr()[posTown2num(town0FC.f, town0FC.c)];
        var town1 = window.gui.getTownArr()[posTown2num(town1FC.f, town1FC.c)];
        if (!town0.isBlocked()) {
            town0.addAllowedColor(color);
            window.g.getData().game.towns[town0FC.f][town0FC.c] = town0.getDataForTownsSQLArr();
        }
        if (!town1.isBlocked()) {
            town1.addAllowedColor(color);
            window.g.getData().game.towns[town1FC.f][town1FC.c] = town1.getDataForTownsSQLArr();
        }
        this.addAllowedColor2RoadsFromTown(town0FC, color);
        this.addAllowedColor2RoadsFromTown(town1FC, color);
    }

    // -------------------------------------------------------------------------

    placeAllocationRound0(cx, cy) {
        var click_detectado = false; // esta variable permite simular un sistema de capas en el click

        var color = window.g.getData().player.color;

        var townI = 0;
        for (var i = 0; i < window.gui.getTownArr().length; i++) {
            if (window.gui.getTownArr()[i].checkClick(cx, cy, color)) {
                window.gui.send1Click2Board();

                window.gui.getTownArr()[i].setTeam(color);
                window.gui.getTownArr()[i].show();

                var ifc = num2posTown(i);
                var allowedColors = window.g.getData().game.towns[ifc.f][ifc.c][3];
                window.g.getData().game.towns[ifc.f][ifc.c] = [color, 1, 0, allowedColors];

                // detectamos si el usuario actual es el último del turno
                if (window.g.getData().player.id == window.g.getData().game.order[window.g.getData().game.order.length - 1]) {
                    window.g.passTurn();
                    window.g.nextPhase();
                } else window.g.passTurn();

                click_detectado = true;
                townI = i;
            }
        }

        if (click_detectado && window.gui.getTownArr()[townI].getLevel() == 1) {
            var ifc = num2posTown(townI);
            // detectamos los pueblos adyacentes y los bloqueamos
            this.detectAndBlockAdjacentTowns(ifc);
            // agregamos nuestro color a la lista de colores permitidos de las carreteras adyacentes a nuestra ciudad
            this.addAllowedColor2RoadsFromTown(ifc, color);
            window.g.setTowns();
            window.g.setDRoads();
            window.g.setVRoads();
        }

        return click_detectado;
    }

    placeAllocationRound1(cx, cy) {
        var click_detectado = false; // esta variable permite simular un sistema de capas en el click

        var color = window.g.getData().player.color;

        var townI = 0;
        if (!this.townBulid) {
            for (var i = 0; i < window.gui.getTownArr().length; i++) {
                if (
                    window.gui.getTownArr()[i].getLevel() == 0 &&
                    window.gui.getTownArr()[i].checkClick(cx, cy, color)
                ) {
                    window.gui.send1Click2Board();
                    this.townBulid = true;

                    window.gui.getTownArr()[i].setTeam(color);
                    window.gui.getTownArr()[i].show();

                    var ifc = num2posTown(i);
                    var allowedColors = window.g.getData().game.towns[ifc.f][ifc.c][3];
                    window.g.getData().game.towns[ifc.f][ifc.c] = [color, 1, 0, allowedColors];

                    // registramos el número de esta ciudad en la base de datos para saber cual es la que nos tiene que dera los recursos iniciales
                    window.g.registerTmpData("last_town", i);

                    click_detectado = true;
                    townI = i;
                }
            }

            if (click_detectado && window.gui.getTownArr()[townI].getLevel() == 1) {
                var ifc = num2posTown(townI);
                // detectamos los pueblos adyacentes y los bloqueamos
                this.detectAndBlockAdjacentTowns(ifc);
                // agregamos nuestro color a la lista de colores permitidos de las carreteras adyacentes a nuestra ciudad
                this.addAllowedColor2RoadsFromTown(ifc, color);
                window.g.setTowns();
                window.g.setDRoads();
                window.g.setVRoads();
            }
        }

        if (this.townBulid && this.roadsBulid < 2) {
            if (!click_detectado) {
                for (var i = 0; i < window.gui.getDroadArr().length; i++) {
                    if (window.gui.getDroadArr()[i].checkClick(cursorX_relM, cursorY_relM, color)) {
                        if (window.gui.getDroadArr()[i].isHidden()) {
                            window.gui.getDroadArr()[i].setTeam(color);
                            window.gui.getDroadArr()[i].show();

                            window.g.getData().game.droads[i] = [color, []];
                            this.addAllowedColor2RoadsFromRoad(window.joc.dRoadTown, i, color);

                            click_detectado = true;
                            this.roadsBulid++;
                        }
                    }
                }
                window.g.setDRoads();
            }
            if (!click_detectado) {
                for (var i = 0; i < window.gui.getVroadArr().length; i++) {
                    if (window.gui.getVroadArr()[i].checkClick(cursorX_relM, cursorY_relM, color)) {
                        if (window.gui.getVroadArr()[i].isHidden()) {
                            window.gui.getVroadArr()[i].setTeam(color);
                            window.gui.getVroadArr()[i].show();

                            window.g.getData().game.vroads[i] = [color, []];
                            this.addAllowedColor2RoadsFromRoad(window.joc.vRoadTown, i, color);

                            click_detectado = true;
                            this.roadsBulid++;
                        }
                    }
                }
                window.g.setVRoads();
            }
            window.g.setTowns();
            window.g.setDRoads();
            window.g.setVRoads();
        }

        return click_detectado;
    }

    // esta función no se usa solo esta aquí para recordar lo que hay que poner para mostrar los demás elementos del tablero
    // activateObjectOnClick(cursorX_relM, cursorY_relM) {
    //
    //     var click_detectado = false; // esta variable permite simular un sistema de capas en el click
    //
    //     var color = window.g.getData().player.color;
    //
    //     var townI = 0;
    //     for (var i = 0; i < window.joc.townArr.length; i++) {
    //         if (window.joc.townArr[i].checkClick(cursorX_relM, cursorY_relM, color)) {
    //             window.gui.send1Click2Board();
    //             if (window.joc.townArr[i].getLevel() == 0) {
    //                 window.joc.townArr[i].setTeam(color);
    //                 window.joc.townArr[i].show();
    //
    //                 var ifc = num2posTown(i);
    //                 window.g.getData().game.towns[ifc.f][ifc.c] = [color, 1, 0];
    //
    //                 if (
    //                     window.g.getData().game.phase == 3 &&
    //                     window.g.getData().game.round == 0
    //                 ) {
    //                     window.g.passTurn();
    //                 }
    //
    //                 click_detectado = true;
    //                 townI = i;
    //             } else if (window.joc.townArr[i].getLevel() == 1) {
    //                 window.joc.townArr[i].upgrade();
    //
    //                 var ifc = num2posTown(i);
    //                 window.g.getData().game.towns[ifc.f][ifc.c] = [color, 2, 0];
    //                 window.g.setTowns();
    //
    //                 click_detectado = true;
    //             }
    //         }
    //     }
    //
    //     // detectamos los pueblos adyacentes y los bloqueamos
    //     if (click_detectado && window.joc.townArr[townI].getLevel() == 1) {
    //         var ifc = num2posTown(townI);
    //         var blockedTowns = getBlockedTowns(ifc.f, ifc.c);
    //         for (var i = 0; i < blockedTowns.length; i++) {
    //             var blockedTownNum = posTown2num(blockedTowns[i].f, blockedTowns[i].c);
    //             window.joc.townArr[blockedTownNum].setBlocked(true);
    //             window.g.getData().game.towns[blockedTowns[i].f][blockedTowns[i].c] = [0, 0, 1];
    //         }
    //         window.g.setTowns();
    //     }
    //
    //     // por el momento bloqueamos los clics a los demás elementos del tablero
    //     if (window.g.getData().game.phase > 3) {
    //         if (!click_detectado) {
    //             for (var i = 0; i < window.joc.droadArr.length; i++) {
    //                 if (window.joc.droadArr[i].checkClick(cursorX_relM, cursorY_relM)) {
    //                     if (window.joc.droadArr[i].isHidden()) {
    //                         window.joc.droadArr[i].setTeam(window.g.getData().player.color);
    //                         window.joc.droadArr[i].show();
    //                         click_detectado = true;
    //                     }
    //                 }
    //             }
    //         }
    //         if (!click_detectado) {
    //             for (var i = 0; i < window.joc.vroadArr.length; i++) {
    //                 if (window.joc.vroadArr[i].checkClick(cursorX_relM, cursorY_relM)) {
    //                     if (window.joc.vroadArr[i].isHidden()) {
    //                         window.joc.vroadArr[i].setTeam(window.g.getData().player.color);
    //                         window.joc.vroadArr[i].show();
    //                         click_detectado = true;
    //                     }
    //                 }
    //             }
    //         }
    //         if (!click_detectado) {
    //             for (var i = 0; i < window.joc.hexArr.length; i++) {
    //                 if (window.joc.hexArr[i].checkClick(cursorX_relM, cursorY_relM) && !window.joc.hexArr[i].getThief()) {
    //                     window.joc.hexArr[i].thiefOn();
    //                     window.joc.thiefPosition = i;
    //                     click_detectado = true;
    //                 }
    //             }
    //             // este segundo for es necesario para que no quede justo el anterior tambien mostrado
    //             for (var i = 0; i < window.joc.hexArr.length; i++) {
    //                 if (window.joc.hexArr[i].getThief() && i != window.joc.thiefPosition) {
    //                     window.joc.hexArr[i].thiefOff();
    //                 }
    //             }
    //         }
    //     }
    //
    //     // dibujamos el tablero de nuevo
    //     if (click_detectado) window.gui.drawBoard();
    //
    // }
}
