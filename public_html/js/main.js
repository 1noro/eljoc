
function main() {
    console.log("[MAIN] Start...");
    getHTMLElements();
    checkCompatibility();
    // creamos y asignamos las interfaces de usuario (por comados y gráfica)
    window.cli = new CLI();
    window.gui = new GUI();
    // creamos una partida con los datos por defecto
    window.g = new Game(window.joc.defaultData, window.joc.title);
    // --- iniciamos el timer --------------------------------------------------
    // window.joc.timer = setInterval(myTimer, window.joc.refreshInterval);
    // --- empezamos a capturar los eventos de teclado y ratón -----------------
    document.onmousedown = getCursolRelAndDoSomething;
}
