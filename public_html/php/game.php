
<?php

    function join_game($OUT, $dbdata) {
        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        $sql = "CALL join_game(" . $OUT["data"]->player->id . ", " . $OUT["data"]->game->id . ", @ok)";
        $result = $conn->query($sql);
        $sql = "SELECT @ok AS 'joined'";
        $result = $conn->query($sql);
        if (isset($result)) {
            $row = mysqli_fetch_array($result);
            $OUT["data"]->game->joined = intval($row['joined']);
            if ($row['joined'] == 1) {
                $OUT["exec"] = "join_ok";
                $OUT["data"]->game->joined = 1;
                array_push($OUT["info"], "el jugador " . $OUT["data"]->player->id . " se unió a la partida " . $OUT["data"]->game->id . "");
                $OUT = getGameStatus($OUT, $conn);
            } else {
                $OUT["exec"] = "join_fail";
                $OUT["data"]->game->joined = 0;
                array_push($OUT["info"], "el jugador " . $OUT["data"]->player->id . " no puede unirse a la partida " . $OUT["data"]->game->id . " porque la partida esta llena o iniciada");
            }

        } else array_push($OUT["error"], "(join_game) result not set: " . $conn->error . "");
        $conn->close();
        return $OUT;
    }

    function set_ready($OUT, $dbdata) {
        $OUT["exec"] = "set_ready";
        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        $sql = "CALL set_ready(" . $OUT["data"]->player->id . ", " . $OUT["data"]->game->id . ")";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function request_turn($OUT, $dbdata) {
        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        $sql = "CALL request_turn(" . $OUT["data"]->player->id . ", " . $OUT["data"]->game->id . ")";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function pass_turn($OUT, $dbdata) {
        $next_player = 0;
        $next_game_round = false;

        for ($i=0; $i < count($OUT["data"]->game->order); $i++) {
            if ($OUT["data"]->player->id == $OUT["data"]->game->order[$i]) {
                if ($i + 1 >= count($OUT["data"]->game->order)) {
                    $next_player = $OUT["data"]->game->order[0];
                    $next_game_round = true;
                } else $next_player = $OUT["data"]->game->order[$i + 1];
                break;
            }
        }

        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        if ($next_game_round) {
            $sql = "CALL next_game_round(" . $OUT["data"]->game->id . ")";
            $result = $conn->query($sql);
        }

        $sql = "CALL request_turn(" . $next_player . ", " . $OUT["data"]->game->id . ")";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function next_phase($OUT, $dbdata) {
        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        $sql = "CALL next_phase(" . $OUT["data"]->game->id . ")";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function set_order($OUT, $dbdata) {
        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        $sql = "CALL set_order(" . $OUT["data"]->game->id . ", '" . json_encode($OUT["data"]->game->order) . "')";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function set_towns($OUT, $dbdata) {
        $conn = getConnection($dbdata);

        $sql = "CALL set_towns(" . $OUT["data"]->game->id . ", '" . json_encode($OUT["data"]->game->towns) . "')";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function set_droads($OUT, $dbdata) {
        $conn = getConnection($dbdata);

        $sql = "CALL set_droads(" . $OUT["data"]->game->id . ", '" . json_encode($OUT["data"]->game->droads) . "')";
        $result = $conn->query($sql);

        // array_push($OUT["info"], json_encode($OUT["data"]->game->droads));
        // array_push($OUT["info"], "(set_droads): " . $conn->error);

        $conn->close();
        return $OUT;
    }

    function set_vroads($OUT, $dbdata) {
        $conn = getConnection($dbdata);

        $sql = "CALL set_vroads(" . $OUT["data"]->game->id . ", '" . json_encode($OUT["data"]->game->vroads) . "')";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function start_game($OUT, $dbdata) {
        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        $sql = "CALL start_game(" . $OUT["data"]->player->id . ", " . $OUT["data"]->game->id . ", '" . json_encode($OUT["data"]->game->order) . "')";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

    function register_tmp_data($OUT, $dbdata) {
        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        for ($i=0; $i < count($OUT["data"]->tmp); $i++) {
            $sql = "CALL register_tmp_data(" . $OUT["data"]->game->id . ", " . $OUT["data"]->player->id . ", '" . $OUT["data"]->tmp[$i]->tag . "', '" . $OUT["data"]->tmp[$i]->data . "');";
            $result = $conn->query($sql);
        }

        $conn->close();
        return $OUT;
    }

    function end_phase_2($OUT, $dbdata) {
        $conn = getConnection($dbdata);

        for ($i=0; $i < count($OUT["data"]->tmp); $i++) {
            $sql = "CALL register_tmp_data(" . $OUT["data"]->game->id . ", " . $OUT["data"]->player->id . ", '" . $OUT["data"]->tmp[$i]->tag . "', '" . $OUT["data"]->tmp[$i]->data . "');";
            $result = $conn->query($sql);
        }

        $conn->close();
        $conn = getConnection($dbdata);

        $sql = "CALL get_turn_dices(" . $OUT["data"]->game->id . ")";
        $result = $conn->query($sql);

        $id_player = array();
        $dicesSum = array();
        if (isset($result)) {
            if ($result->num_rows > 0) {

                while($row = $result->fetch_assoc()) {
                    array_push($id_player, intval($row["id_player"]));
                    array_push($dicesSum, intval($row["tmp_data"]));
                }

                array_multisort($dicesSum, $id_player);
                array_push($OUT["info"], "new_order: " . json_encode($id_player));

            } else {
                array_push($OUT["info"], "(end_phase_2 > get_turn_dices) 0 resultados");
            }
        } else array_push($OUT["error"], "(end_phase_2 > get_turn_dices) result not set: " . $conn->error . "");

        $conn->close();

        $conn = getConnection($dbdata);
        $sql = "CALL end_phase2(" . $OUT["data"]->game->id . ", " . $id_player[0] . ", '" . json_encode($id_player) . "');";
        $result = $conn->query($sql);

        // array_push($OUT["info"], "(end_phase_2 > end_phase2): " . $conn->error);

        $conn->close();
        return $OUT;
    }

?>
