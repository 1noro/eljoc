
<?php

    function ping_and_check($OUT, $dbdata) {
        $OUT["exec"] = "ping_and_check";

        $conn = getConnection($dbdata);
        ping($OUT, $conn);

        $OUT = getGameStatus($OUT, $conn);

        $conn->close();
        return $OUT;
    }

?>
