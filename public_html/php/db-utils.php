
<?php

    function getConnection($dbdata) {
        $conn = new mysqli($dbdata["server"], $dbdata["user"], $dbdata["pass"], $dbdata["db"]);
        // Check connection
        if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);
        return $conn;
    }

    function delete_all_data($OUT, $dbdata) {
        $conn = getConnection($dbdata);

        $sql = "CALL delete_all_data;";
        $result = $conn->query($sql);

        $conn->close();
        return $OUT;
    }

?>
