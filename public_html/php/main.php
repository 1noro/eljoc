<?php

    // Incluimos las credenciales de la base de datos
    include 'db-data.php';
    include 'db-utils.php';
    include 'game-utils.php';
    include 'ping-utils.php';
    include 'game.php';
    include 'ping.php';

    // Definimos las variables globales
    $OUT = array();
    $OUT["info"] = array(); // lista de mensajes del PHP para verlos en el JS
    $OUT["error"] = array(); // lista de errores del PHP para verlos en el JS
    $OUT["exec"] = "overwrite_d"; // función que ejucutará la clase Game en el JS
    $OUT["data"] = 0; // la estructura de información de la partida

    $execOk = false; // si la variable exec recibida por post es correcta
    $dataOk = false; // si la variable data recibida por post es correcta

    // Comprobamos si hay errores en el mensage recivido del JS
    if (isset($_POST["exec"])) $execOk = true;
        else array_push($OUT["error"], "exec not defined");
    if (isset($_POST["data"])) {
        $OUT["data"] = json_decode(base64_decode($_POST["data"]));
        if ($OUT["data"] === null && json_last_error() !== JSON_ERROR_NONE)
            array_push($OUT["error"], "data is not JSON formated");
        else $dataOk = true;
    } else array_push($OUT["error"], "data not defined");

    // Ejecutamos codigo en función de lo pedido desde el JS
    if ($execOk && $dataOk) {
        // array_push($OUT["info"], "exec and data well formated");
        if ($_POST["exec"] == "ping") {
            $OUT = ping($OUT, $dbdata);
        } else if ($_POST["exec"] == "check_ping") {
            $OUT = check_ping($OUT, $dbdata);
        } else if ($_POST["exec"] == "ping_and_check") {
            $OUT = ping_and_check($OUT, $dbdata);
        } else if ($_POST["exec"] == "join_game") {
            $OUT = join_game($OUT, $dbdata);
        } else if ($_POST["exec"] == "set_ready") {
            $OUT = set_ready($OUT, $dbdata);
        } else if ($_POST["exec"] == "request_turn") {
            $OUT = request_turn($OUT, $dbdata);
        } else if ($_POST["exec"] == "pass_turn") {
            $OUT = pass_turn($OUT, $dbdata);
        } else if ($_POST["exec"] == "next_phase") {
            $OUT = next_phase($OUT, $dbdata);
        } else if ($_POST["exec"] == "set_order") {
            $OUT = set_order($OUT, $dbdata);
        } else if ($_POST["exec"] == "start_game") {
            $OUT = start_game($OUT, $dbdata);
        } else if ($_POST["exec"] == "register_tmp_data") {
            $OUT = register_tmp_data($OUT, $dbdata);
        } else if ($_POST["exec"] == "end_phase_2") {
            $OUT = end_phase_2($OUT, $dbdata);
        } else if ($_POST["exec"] == "set_towns") {
            $OUT = set_towns($OUT, $dbdata);
        } else if ($_POST["exec"] == "set_droads") {
            $OUT = set_droads($OUT, $dbdata);
        } else if ($_POST["exec"] == "set_vroads") {
            $OUT = set_vroads($OUT, $dbdata);
        } else if ($_POST["exec"] == "delete_all_data") {
            $OUT = delete_all_data($OUT, $dbdata);
        } else array_push($OUT["error"], "valor de exec no reconocible: (" . $_POST["exec"] . ")");
    }

    echo base64_encode(json_encode($OUT));

?>
