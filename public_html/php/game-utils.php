
<?php

    function getGameStatus($OUT, $conn) {

        // vaciamos la sección tmp de data
        $OUT["data"]->tmp = array();

        $sql = "SELECT waiting, finished, phase, game_round, game_order, towns, droads, vroads FROM game WHERE id = " . $OUT["data"]->game->id . ";";
        $result = $conn->query($sql);

        if (isset($result)) {
            $row = mysqli_fetch_array($result);
            $OUT["data"]->game->waiting = intval($row['waiting']);
            $OUT["data"]->game->finished = intval($row['finished']);
            $OUT["data"]->game->phase = intval($row['phase']);
            $OUT["data"]->game->round = intval($row['game_round']);
            $OUT["data"]->game->order = json_decode($row['game_order']);
            $OUT["data"]->game->towns = json_decode($row['towns']);
            $OUT["data"]->game->droads = json_decode($row['droads']);
            $OUT["data"]->game->vroads = json_decode($row['vroads']);
        } else array_push($OUT["error"], "(select * from game) result not set: " . $conn->error . "");

        // ---------------------------------------------------------------------
        $sql = "CALL get_game_status(" . $OUT["data"]->game->id . ")";
        $result = $conn->query($sql);

        $OUT["data"]->opponents = array();
        if (isset($result)) {
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    if ($OUT["data"]->player->id == intval($row["id_player"])) {
                        $OUT["data"]->player->color = intval($row["color"]);
                        $OUT["data"]->player->online = intval($row["online"]);
                        $OUT["data"]->player->ready = intval($row["ready"]);
                        $OUT["data"]->player->turn = intval($row["turn"]);
                    } else {
                        array_push($OUT["data"]->opponents, array(
                            "id" => intval($row["id_player"]),
                            "color" => intval($row["color"]),
                            "online" => intval($row["online"]),
                            "ready" => intval($row["ready"]),
                            "turn" => intval($row["turn"])
                        ));
                    }
                }
            } else {
                array_push($OUT["info"], "(get_game_status) 0 resultados");
            }
        } else array_push($OUT["error"], "(get_game_status) result not set: " . $conn->error . "");
        return $OUT;
    }

?>
