<?php

    include 'dbdata.php';

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($conn->ping()) {
        echo "Our connection is ok!<br>";
    } else {
        echo "Error: " . $mysqli->error . "<br>";
    }

    echo "Host info: " . mysqli_get_host_info($conn) . "<br>";

    $sql = "SELECT id FROM player";
    $result = $conn->query($sql);
    if (isset($result)) {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                echo "id: " . $row["id"] . "<br>";
            }
        } else {
            echo "0 results" . "<br>";
        }
    } else {
        echo "Nop: " . $conn->error . "<br>";
    }

    $conn->close();
?>
