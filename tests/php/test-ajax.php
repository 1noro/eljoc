<?php
    if (isset($_POST["exec"])) {
        if ($_POST["exec"] == "hello") {
            echo base64_encode('{"text" : "Hello from PHP!!"}');
        } else if ($_POST["exec"] == "echo") {
            if (isset($_POST["data"])) {
                echo base64_encode('{"text" : "ECHO: ' . base64_decode($_POST["data"]) . '"}');
            } else {
                echo  base64_encode('{"text" : "data not defined"}');
            }
        } else {
            echo  base64_encode('{"text" : "exec valor no reconocible: (' . $_POST["exec"] . ')"}');
        }
    } else {
        echo  base64_encode('{"text" : "exec not defined"}');
    }
?>
