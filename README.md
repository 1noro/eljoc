# elJoc

[![HTML 5](https://img.shields.io/badge/HTML-5-%23e44d26)](https://www.w3schools.com/html/html5_intro.asp)
[![CSS 3](https://img.shields.io/badge/CSS-3-%23146eb0)](https://www.w3schools.com/css/default.asp)
[![ECMAScript 2015](https://img.shields.io/badge/ECMAScript-2015-%23cabc13)](https://en.wikipedia.org/wiki/ECMAScript#6th_Edition_-_ECMAScript_2015)
[![PHP >=7.0](https://img.shields.io/badge/PHP-%3E%3D7.0-%234984bc)](https://www.php.net/releases/index.php)
[![MariaDB 15.1](https://img.shields.io/badge/MariaDB-15.1-lightgrey)](https://mariadb.org/)

Copia flagrante del CATAN, el juego.

Juego de 3 a 4 jugadores on-line, no necesita registro, solo invéntate un número para identificarte y acuerda con tus amigos un número de partida.

Programada en JavaScript, PHP y SQL bajo un servidor MariaDB.

# To-do list

- Unificar el paso al SQL de los 3 arrays grandes (towns, droads y vroads) para que se realicen en una misma transacción en las ocasiones que sea conveniente actualizarlas todas a la vez
- Cartas de caballeros y caminos a la izquierda de los oponentes en ved de abajo (considerar)
- Hacer un diagrama de clases para organizar un poco las funciones de cada una
- Invertir el orden de los dados en la fase 2 (de mayor a menor) (en el PHP)
- Dividir la clase Opponent en 2 por una parte la parte del juego y por otra la parte grafica de la GUI (OpponentItem)
- Paquetes de ping diferentes dependiendo de la fase, para no estar siempre actualizando un montón de información de golpe
- Cambiar el board de div y otorgarle el suyo propio, de esta forma quedará el MiddlePannel separado del Board y podremos crearlo y destruirlo a placer
- Hacer que el LoginForm genere los proios HTML objects y agregar un comportamiento de testing para que genere un usuario y una partida aleatorias para no estar poniendo siempre los números en el formulario de inicio de sesión
- Hacer un procedimiento que permita mediante CLI hacer un reset de la partida y de toda la base de datos, haciendo que todos los jugadores recarguen su pagina
- Implementar una variable en la información que se envía desde el PHP que valide el estado de la base de datos en cada ping, así se podrá saber cuando deben recargarse todas las paginas

## Languages avaliable

- __es.ES__ Spanish (Spain).

## Things that I would like to add in the future

- Add more players

## Credits

- Art and design by Kuria.
- Created, programmed and maintained by [1nor0](https://gitlab.com/1nor0).

## Licenses

[![GPL3](https://img.shields.io/badge/license-GPL3-green.svg)](https://www.gnu.org/licenses/gpl.txt)
[![CC BY 4.0](https://img.shields.io/badge/license-CC%20BY%204.0-green.svg)](https://creativecommons.org/licenses/by/4.0/)

- Project under the __[GNU General Public License version 3](https://www.gnu.org/licenses/gpl.txt)__.
- The icons sets are under the __[Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/)__ (CC BY 4.0) license .

## Donations

- __Bitcoin:__ 37Cx8i8Q4VjJJpMX6oRYVh2FUpXR1yMf54
- __Litecoin:__ MMUTvmaiZhPHjK68Jy1Z9roVFo7siGCcf8
- __Dogecoin:__ DSaqBstRo4h6dpzs9n7UDv39cg9wqErZvo
