
# Códigos numéricos del juego (a nivel de programación)

### Fases de la partida (Phases)
0. Espera de usuarios
1. Inicio de la partida
2. Ronda de asignación de turno
3. Ronda de asignación de lugares 0
4. Ronda de asignación de lugares 1
5. Juego
6. Fin del juego

### Colores / Equipos (Colors / Teams)
0. Nadie / Especial / Comodín / Random
1. Amarillo
2. Azul
3. Morado
4. Rojo

### Resursos (Resources)
0. Desierto / Especial / Comodín
1. Trigo
2. Piedra
3. Oveja
4. Arcilla
5. Madera

### Puertos (Harbor)
0. 3:1 - 3 cartas iguales de cualquier tipo por 1 a elegir de la reserva
1. 2:1 Trigo - 2 cartas de Trigo por 1 a elegir de la reserva
2. 2:1 Piedra - 2 cartas de Piedra por 1 a elegir de la reserva
3. 2:1 Oveja - 2 cartas de Oveja por 1 a elegir de la reserva
4. 2:1 Arcilla - 2 cartas de Arcilla por 1 a elegir de la reserva
5. 2:1 Madera - 2 cartas de Madera por 1 a elegir de la reserva
6. Ningún puerto

### Desarrollo (Development)
0. Caballero - Mueve el ladrón, roba una carta de tu oponente y agrégala a tu colección de caballeros
1. Carreteras - Construye 2 caminos sin coste alguno
2. Invento - Roba 2 cartas de recursos a tu elección de la banca
3. Punto de victoria - Esta carta suma un punto a tus puntos
4. Monopolio - Elige un recurso y obtén todas las cartas de ese recurso que haya en las manos de tus oponentes

### window.joc.defaultData.game.towns (Explicación del Array)

nodo = [0, 0, 0, []]

nodo = [colorId, levelId, blocked, allowedColors]

- colorId - Integer (1, 2, 3, 4)   (Amarillo, Azul, Morado, Rojo)
- levelId - Integer (0, 1, 2)      (Oculta, Pueblo, Ciudad)
- blocked - Integer (0, 1)         (Si, No)
- allowedColors - Integer Array
